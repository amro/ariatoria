#version 140

out vec4 color;

uniform vec4 in_color;

void main() {
  color = in_color;
}
