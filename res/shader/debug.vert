#version 140

in vec2 position;

uniform mat4 ortho;
uniform vec2 offset;

void main() {
  gl_Position = vec4(position + offset, 0.0, 1.0) * ortho;
}
