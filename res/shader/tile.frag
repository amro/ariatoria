
#version 140

in vec2 v_tex_coords;
out vec4 color;

uniform vec4 in_color;
uniform sampler2D texture;

void main() {
  color = texture2D(texture, v_tex_coords) * in_color;
}
