#version 140

in vec2 position;
in vec2 coords;
out vec2 v_tex_coords;

uniform mat4 ortho;
uniform vec2 offset;

void main() {
  gl_Position = vec4(position + offset, 0.0, 1.0) * ortho;
  v_tex_coords = coords;
}
