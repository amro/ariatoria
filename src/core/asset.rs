
use crate::generational_index::Array;
use crate::generational_index::Allocator;
use crate::generational_index::Index;

use std::{collections::HashMap, marker::PhantomData};

#[derive(Clone, Copy, Debug)]
pub enum Error {
  NotFound,
  InvalidFormat,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Handle<A>(pub Index, PhantomData<A>);

impl <A> Handle<A> {
  pub fn of(id: Index) -> Self {
    Handle(
      id,
      PhantomData
    )
  }
}

// we have to implement Clone/Copy manually here,
// since the derived version places a bound A: Copy
// due to the PhantomData. However since we don't
// actually hold an A, we can safely clone/copy.
impl <A> Clone for Handle<A> {
  fn clone(&self) -> Handle<A> {
    Handle(self.0, PhantomData)
  }
}
impl <A> Copy for Handle<A> { }

pub struct Assets<A> {
  allocator: Allocator,
  array: Array<A>,
}

pub fn name(spec: &'_ str) -> &'_ str {
  let mut start = 0;
  let mut end = spec.len();
  if let Some(slash) = spec.rfind('/') {
    start = slash + 1;
  }
  if let Some(dot) = spec.rfind('.') {
    end = dot;
  }
  &spec[start..end]
}

pub struct CachedAssets<A> {
  assets: Assets<A>,
  lookup: HashMap<String, Handle<A>>,
}

impl <A> CachedAssets<A> {
  pub fn new() -> Self {
    CachedAssets {
      assets: Assets::new(),
      lookup: HashMap::new(),
    }
  }

  pub fn get_or_load<F>(&mut self, spec: &str, fun: F) -> Handle<A>
  where F: FnOnce(&str) -> A
   {
     if self.lookup.contains_key(spec) {
       *self.lookup.get(spec).unwrap()
     } else {
       let asset = fun(spec);
       let handle = self.assets.allocate();
       self.assets.put(handle, asset);
       handle
     }
  }
  
  pub fn get(&self, handle: Handle<A>) -> Option<&A> {
    self.assets.get(handle)
  }
}

impl <A> Assets<A> {
  pub fn new() -> Self {
    Assets {
      allocator: Allocator::new(),
      array: Array::new(),
    }
  }

  pub fn allocate(&mut self) -> Handle<A> {
    Handle::of(self.allocator.allocate())
  }

  pub fn put(&mut self, handle: Handle<A>, value: A) {
    self.array.insert(handle.0, value);
  }

  pub fn get(&self, handle: Handle<A>) -> Option<&A> {
    self.array.get(handle.0)
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn test_name() {
    assert_eq!(name("foo.txt"), "foo");
    assert_eq!(name("/path/bar.rs"), "bar");
    assert_eq!(name("foo"), "foo");
  }
}
