use crate::core::Handle;
use crate::gfx::sprite::SpriteVerts;
use crate::gfx::texture::Texture;
use crate::logic::character::Character;
use crate::math::Collider;
use crate::gfx::sprite::Sprite;
use crate::gfx::render::BufferRef;
use crate::gfx::debug::DebugVerts;
use crate::gfx::debug::DebugBox;
use crate::gfx::camera::Camera;
use crate::gfx::animation::Animation;
use crate::generational_index::ArrayIterMut;
use crate::generational_index::ArrayIter;
use crate::generational_index::Array;

use super::{Transform, world::Entity};

pub struct ComponentArray<T>(Array<T>);

pub struct EntityIter<'a, T> {
  inner: ArrayIter<'a, T>,
}

impl <'a, T: 'a> Iterator for EntityIter<'a, T> {
  type Item = (Entity, &'a T);

  fn next(&mut self) -> Option<Self::Item> {
    while let Some((index, entry)) = self.inner.next() {
      return Some((Entity(index), entry));
    }
    None
  }
}

pub struct EntityIterMut<'a, T> {
  inner: ArrayIterMut<'a, T>,
}

impl <'a, T: 'a> Iterator for EntityIterMut<'a, T> {
  type Item = (Entity, &'a mut T);

  fn next(&mut self) -> Option<Self::Item> {
    while let Some((index, entry)) = self.inner.next() {
      return Some((Entity(index), entry));
    }
    None
  }
}

impl <T> ComponentArray<T> {
  pub fn new() -> Self {
    ComponentArray(Array::new())
  }

  pub fn has_component(&self, entity: Entity) -> bool {
    self.0.contains_key(entity.0)
  }

  pub fn insert(&mut self, entity: Entity, component: T) {
    self.0.insert(entity.0, component);
  }

  pub fn get(&self, entity: Entity) -> Option<&T> {
    self.0.get(entity.0)
  }

  pub fn get_mut(&mut self, entity: Entity) -> Option<&mut T> {
    self.0.get_mut(entity.0)
  }

  pub fn iter(&self) -> EntityIter<'_, T>{
    EntityIter { inner: self.0.iter() }
  }

  pub fn iter_mut(&mut self) -> EntityIterMut<'_, T> {
    EntityIterMut { inner: self.0.iter_mut() }
  }
}

pub struct Components {
  pub animation: ComponentArray<Animation>,
  pub buffer_ref: ComponentArray<BufferRef>,
  pub camera: ComponentArray<Camera>,
  pub collider: ComponentArray<Collider>,
  pub character: ComponentArray<Character>,
  pub debug_box: ComponentArray<DebugBox>,
  pub debug_verts: ComponentArray<DebugVerts>,
  pub sprite: ComponentArray<Sprite>,
  pub sprite_verts: ComponentArray<SpriteVerts>,
  pub texture: ComponentArray<Handle<Texture>>,
  pub transform: ComponentArray<Transform>,
}

impl Components {
  pub fn new() -> Self {
    Components {
      animation: ComponentArray::new(),
      buffer_ref: ComponentArray::new(),
      camera: ComponentArray::new(),
      collider: ComponentArray::new(),
      character: ComponentArray::new(),
      debug_box: ComponentArray::new(),
      debug_verts: ComponentArray::new(),
      sprite: ComponentArray::new(),
      sprite_verts: ComponentArray::new(),
      texture: ComponentArray::new(),
      transform: ComponentArray::new(),
    }
  }
}