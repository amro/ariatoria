use gilrs::EventType;
use gilrs::{Axis, Button};

use glutin::event::ElementState;
use glutin::event::VirtualKeyCode;

use std::collections::HashMap;

#[derive(Debug, Clone, Copy)]
#[repr(usize)]
pub enum InputCommand {
  MoveUp = 0,
  MoveLeft = 1,
  MoveDown,
  MoveRight,
  PanUp,
  PanLeft,
  PanDown,
  PanRight,
  ZoomIn,
  ZoomOut,
}

#[derive(Debug, Clone, Copy)]
#[repr(usize)]
pub enum InputAxis {
  LeftX,
  LeftY,
  RightX,
  RightY
}

#[derive(Debug, Clone)]
pub struct InputState {
  state: Vec<bool>,
  axes: Vec<f32>,
}

pub struct Input {
  inner: InputState,
  controls: HashMap<Button, InputCommand>,
  keys: HashMap<VirtualKeyCode, InputCommand>,
  axes: HashMap<Axis, InputAxis>,
}

impl InputState {
  pub fn new() -> Self {
    InputState {
      state: Vec::new(),
      axes: Vec::new(),
    }
  }

  pub fn get_state(&self, cmd: InputCommand) -> bool {
    self.state[cmd as usize]
  }

  pub fn set_state(&mut self, cmd: &InputCommand, s: bool) {
    self.state[*cmd as usize] = s;
  }

  pub fn get_axis(&self, axis: InputAxis) -> f32 {
    self.axes[axis as usize]
  }

  pub fn set_axis(&mut self, axis: InputAxis, value: f32) {
    self.axes[axis as usize] = value;
  }
}

impl Input {
  pub fn new_p1() -> Self {
    let controls = HashMap::new();

    let mut axes = HashMap::new();
    axes.insert(Axis::LeftStickX, InputAxis::LeftX);
    axes.insert(Axis::LeftStickY, InputAxis::LeftY);
    
    let mut keys = HashMap::new();
    keys.insert(VirtualKeyCode::W, InputCommand::PanUp);
    keys.insert(VirtualKeyCode::A, InputCommand::PanLeft);
    keys.insert(VirtualKeyCode::S, InputCommand::PanDown);
    keys.insert(VirtualKeyCode::D, InputCommand::PanRight);

    keys.insert(VirtualKeyCode::PageUp, InputCommand::ZoomIn);
    keys.insert(VirtualKeyCode::PageDown, InputCommand::ZoomOut);
    
    Input {
      inner: InputState {
        state: vec![false; 16],
        axes: vec![0.0; 16],
      },
      controls,
      keys,
      axes,
    }
  }

  pub fn new_p2() -> Self {
    let controls = HashMap::new();
    let mut axes = HashMap::new();

    axes.insert(Axis::RightStickX, InputAxis::LeftX);
    axes.insert(Axis::RightStickY, InputAxis::LeftY);
    
    let keys = HashMap::new();
    
    Input {
      inner: InputState {
        state: vec![false; 16],
        axes: vec![0.0; 16],
      },
      controls,
      keys,
      axes,
    }
  }


  pub fn clone_inner(&self) -> InputState {
    self.inner.clone()
  }


  pub fn handle_key(&mut self, key: VirtualKeyCode, state: ElementState) {
    match state {
      ElementState::Pressed => {
        if let Some(cmd) = self.keys.get(&key) {
          self.inner.set_state(cmd, true);
        }
      },
      ElementState::Released => {
        if let Some(cmd) = self.keys.get(&key) {
          self.inner.set_state(cmd, false);
        }
      }
    }
  }

  pub fn handle_event(&mut self, event: gilrs::Event) {
    match event.event {
      EventType::ButtonPressed(button, _) => {
        if let Some(cmd) = self.controls.get(&button) {
          self.inner.set_state(cmd, true);
        }
      },
      EventType::ButtonReleased(button, _) => {
        if let Some(cmd) = self.controls.get(&button) {
          self.inner.set_state(cmd, false);
        }
      },
      EventType::AxisChanged(axis, amt, _) => {
        if let Some(a) = self.axes.get(&axis) {
          self.inner.set_axis(*a, amt);
        }
      }
      _ => {

      }
    }
  }
}
