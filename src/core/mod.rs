pub mod asset;
mod components;
pub mod input;
pub mod transform;
pub mod resources;
pub mod world;

pub use asset::Assets;
pub use asset::Handle;

pub use input::Input;
pub use input::InputState;
pub use input::InputCommand;

pub use transform::Transform;

pub use resources::Resources;

pub use world::World;

pub trait System {
  fn run(&mut self, resources: &mut Resources, world: &mut World) -> ();
}
