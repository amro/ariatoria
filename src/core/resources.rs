use crate::gfx::texture::TextureSystem;
use crate::math::Matrix;
use crate::game::WindowSize;
use crate::game::ScaleFactor;
use crate::map::Tsx;
use crate::map::Tmx;
use crate::gfx::buffer::Buffers;

use super::{asset::CachedAssets, InputState};

pub struct Resources {
  pub buffers: Buffers,
  pub input: InputState,
  pub ortho_game: Matrix,
  pub ortho_ui: Matrix,
  pub scale_factor: ScaleFactor,
  pub texture_system: TextureSystem,
  pub tmx: CachedAssets<Tmx>,
  pub tsx: CachedAssets<Tsx>,
  pub window_size: WindowSize,
}

impl Resources {
  pub fn new() -> Self {
    Resources {
      buffers: Buffers::new(),
      input: InputState::new(),
      ortho_game: Matrix::simple_ortho(0.0 as f32, 0.0 as f32),
      ortho_ui: Matrix::simple_ortho(0.0 as f32, 0.0 as f32),
      scale_factor: ScaleFactor(1.0),
      texture_system: TextureSystem::new(),
      tmx: CachedAssets::<Tmx>::new(),
      tsx: CachedAssets::<Tsx>::new(),
      window_size: WindowSize(0.0, 0.0),
    }
  }
}