use crate::math::Rect;
use crate::{math::Vector2};

#[derive(Clone, Default)]
pub struct Transform {
  pub position: Vector2,
  pub size: Vector2,
}

impl Transform {
  pub fn new(x: f32, y: f32, width: f32, height: f32) -> Self {
    Transform {
      position: Vector2::new(x, y),
      size: Vector2::new(width, height),
    }
  }
  
  pub fn from_rect(rect: Rect<f32>) -> Self {
    Self::new(rect.x, rect.y, rect.width, rect.height)
  }

  pub fn of(x: f32, y: f32) -> Self {
    Transform {
      position: Vector2::new(x, y),
      size: Vector2::new(1.0, 1.0),
    }
  }

  pub fn zero() -> Self {
    Transform {
      position: Vector2::new(0.0, 0.0),
      size: Vector2::new(0.0, 0.0),
    }
  }
}