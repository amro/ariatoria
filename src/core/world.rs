
use crate::generational_index::{Array, Allocator, Index};

use super::components::Components;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Entity(pub Index);


pub struct World {
  allocator: Allocator,
  entities: Array<Entity>,
  pub components: Components,
}


impl World {
  pub fn new() -> Self {
    World {
      allocator: Allocator::new(),
      entities: Array::new(),
      components: Components::new(),
    }
  }

  pub fn new_entity(&mut self) -> Entity {
    let entity = Entity(self.allocator.allocate());
    self.entities.insert(entity.0, entity);
    entity
  }
}