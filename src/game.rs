
use crate::gfx::debug::DebugSystem;
use crate::gfx::sprite::SpriteSystem;
use crate::js::Runtime;
use crate::map::loader::MapLoader;
use crate::logic::character::CharacterSystem;
use crate::core::Resources;
use crate::core::Input;
use crate::core::World;
use crate::core::Transform;
use crate::gfx::camera::Camera;
use crate::gfx;
use crate::gfx::camera;

use byte_strings::c_str;
use gilrs::Gilrs;
use glutin::event::ElementState;
use glutin::event::MouseButton;
use glutin::event::VirtualKeyCode;
use pathfinder_geometry::vector::Vector2F;

#[derive(Clone, Copy)]
pub struct ScaleFactor(pub f32);

#[derive(Clone, Copy)]
pub struct WindowSize(pub f32, pub f32);

pub struct Game {
  character_system: CharacterSystem,
  debug_system: DebugSystem,
  gilrs: Gilrs,
  input: Input,
  map_loader: MapLoader,
  resources: Resources,
  runtime: Runtime,
  sprite_system: SpriteSystem,
  world: World,
}

impl Game {
  pub fn new(gilrs: Gilrs, runtime: Runtime) -> Self {
    let resources = Resources::new();

    let mut world = World::new();

    let debug_system = DebugSystem::new();
    let character_system = CharacterSystem::new();
    let sprite_system = SpriteSystem::new();
    let map_loader = MapLoader::new();

    let camera = Camera::new();
    let transform = Transform::zero();

    let camera_ent = world.new_entity();
    world.components.camera.insert(camera_ent, camera);
    world.components.transform.insert(camera_ent, transform);

    let input = Input::new_p1();

    Game {
      character_system,
      debug_system,
      gilrs,
      input,
      map_loader,
      resources,
      runtime,
      sprite_system,
      world,
    }
  }

  pub fn init(&mut self) {
    unsafe {
      let ctx = self.runtime.get_context();
      let mut ui_classes = Vec::new();
      ui_classes.append(&mut crate::gfx::ui::get_canvas_classes(ctx));
      ui_classes.append(&mut crate::gfx::ui::get_ui_classes(ctx));
      self.runtime.register_module(c_str!("math"), crate::math::bindings::get_classes(ctx));
      self.runtime.register_module(c_str!("ui"), ui_classes);
      self.runtime.register_module(c_str!("color"), crate::gfx::color::bindings::get_classes(ctx));
    }
    self.runtime.load("ts/build/main.js").unwrap();
  }

  pub fn resize(&mut self, width: u32, height: u32) {
    self.resources.window_size = WindowSize(width as f32, height as f32);
    unsafe {
      let v = self.runtime.get_global(c_str!("Resize"));
      self.runtime.call_arg2(v.value(), width as f32, height as f32);
    }
  }

  pub fn mouse_down(&mut self, _button: MouseButton, _at: Vector2F) {
    unsafe {
      let v = self.runtime.get_global(c_str!("OnMouseDown"));
      self.runtime.call(v.value());
    }
  }

  pub fn mouse_up(&mut self, _button: MouseButton, _at: Vector2F) {
    unsafe {
      let v = self.runtime.get_global(c_str!("OnMouseUp"));
      self.runtime.call(v.value());
    }
  }

  pub fn mouse_move(&mut self, _old: Vector2F, _new: Vector2F) {
    unsafe {
      let v = self.runtime.get_global(c_str!("OnMouseMove"));
      self.runtime.call(v.value());
    }
  }

  pub fn handle_key(&mut self, key: VirtualKeyCode, state: ElementState) {
    self.input.handle_key(key, state);
  }

  pub fn tick(&mut self) {
    while let Some(event) = self.gilrs.next_event() {
      self.input.handle_event(event);
    }
    self.resources.input = self.input.clone_inner();

    gfx::animation::animate_sprites(&mut self.world);
    self.character_system.character_spawn(&mut self.resources, &mut self.world);
    self.character_system.character_movement(&mut self.resources, &mut self.world);

    self.map_loader.load(&mut self.resources, &mut self.world);

    gfx::sprite::sprite_buffer_alloc(&mut self.resources, &mut self.world);
    gfx::sprite::sprite_vert_gen(&mut self.resources, &mut self.world);
    gfx::sprite::upload_sprite_verts(&mut self.world);
    self.sprite_system.render(&mut self.resources, &mut self.world);


    unsafe {
      let v = self.runtime.get_global(c_str!("OnPaint"));
      self.runtime.call(v.value());
    }

    // self.debug_system.debug_buffer_alloc(&mut self.world);
    // self.debug_system.debug_vert_gen(&mut self.resources, &mut self.world);
    // self.debug_system.upload_debug_verts(&mut self.world);
    // gfx::debug::render_debug(&mut self.resources, &mut self.world);

    camera::camera_movement(&mut self.resources, &mut self.world);
    camera::projection_update(&mut self.resources, &mut self.world);
  }
}
