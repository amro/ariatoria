
use crate::core::World;

pub struct Animation {
  current: u32,
  rate: u8,
  counter: u8, 
  coords: Vec<(f32, f32, f32, f32)>,
}

impl Animation {
  pub fn of(coords: Vec<(f32, f32, f32, f32)>) -> Self {
    Animation {
      coords,
      current: 0,
      rate: 8,
      counter: 0
    }
  }
}

pub fn animate_sprites(world: &mut World) {
  for (entity, animation) in world.components.animation.iter_mut() {
    let mut sprite = world.components.sprite.get_mut(entity).unwrap();
    animation.counter += 1;
    if animation.counter >= animation.rate {
      animation.counter = 0;
      animation.current += 1;
      if animation.current >= animation.coords.len() as u32 {
        animation.current = 0;
      }

      sprite.coords = animation.coords[animation.current as usize];
    }
  }
}