use std::collections::HashMap;

use super::render::{BufferId, Renderer};

pub struct Buffer {
  pub id: BufferId,
  pub offset: usize,
}

pub struct Buffers {
  buffers: HashMap<String, Buffer>,
}

impl Buffers {
  pub fn new() -> Self {
    Buffers {
      buffers: HashMap::new(),
    }
  }
  pub fn get_named(&mut self, name: &str) -> &mut Buffer {
    self.buffers.entry(name.into()).or_insert_with(|| {
      Buffer {
        id: Renderer::named_buffer(name, 2048*2048),
        offset: 0,
      }
    })
  }
}