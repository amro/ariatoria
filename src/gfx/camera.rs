
use crate::core::input::InputCommand;
use crate::math::Matrix;
use crate::math::Vector;

use crate::core::{Resources, World};

#[derive(Debug, Clone)]
pub struct Matrices {
  pub ortho_ui: Matrix,
  pub ortho_game: Matrix
}

#[derive(Default)]
pub struct Camera {
  active: bool,
  zoom: f32,
}

impl Camera {
  pub fn new() -> Self {
    Camera {
      active: true,
      zoom: 2.0,
    }
  }
}

pub fn camera_movement(resources: &mut Resources, world: &mut World) {
  let input = &resources.input;
  for (entity, camera) in world.components.camera.iter_mut() {
    if let Some(transform) = world.components.transform.get_mut(entity) {
      if input.get_state(InputCommand::PanUp) {
        transform.position.y += 0.01;
      }
      if input.get_state(InputCommand::PanDown) {
        transform.position.y -= 0.01;
      }
      if input.get_state(InputCommand::PanLeft) {
        transform.position.x -= 0.01;
      }
      if input.get_state(InputCommand::PanRight) {
        transform.position.x += 0.01;
      }
      if input.get_state(InputCommand::ZoomIn) {
        camera.zoom += 0.1;
      }
      if input.get_state(InputCommand::ZoomOut) {
        camera.zoom -= 0.1;
      }
    }
  }
}

pub fn projection_update(resources: &mut Resources, world: &mut World) {
  let window_size = resources.window_size;
  for (entity, camera) in world.components.camera.iter() {
    if let Some(transform) = world.components.transform.get(entity) {
      if camera.active {
        let ortho_tr = Matrix::translation(&Vector::new(transform.position.x, transform.position.y, 0.0));
        let ortho_scale = Matrix::scale(&Vector::new(camera.zoom, camera.zoom, camera.zoom));
        let width = window_size.0;
        let height = window_size.1;
        let base = Matrix::ortho(-width / 2.0, width/2.0, -height/2.0, height/2.0, -1.0, 1.0);
        resources.ortho_game = base * ortho_tr.inverse() * ortho_scale;
        resources.ortho_ui = Matrix::simple_ortho(width, height);
      }
    }
  }
}
