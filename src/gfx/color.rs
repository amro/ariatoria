
#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Color {
  pub r: u8,
  pub g: u8,
  pub b: u8,
  pub a: u8,
}

impl Color {
  pub const WHITE: Color = Color::from_rgb(255, 255, 255);

  pub const fn from_rgba(r: u8, g: u8, b: u8, a: u8) -> Self {
    Color { r, g, b, a }
  }

  pub const fn from_rgb(r: u8, g: u8, b: u8) -> Self {
    Color { r, g, b, a: 255u8 }
  }

  pub fn from_hex(hex: &str) -> Self {
    let start = if hex.starts_with("#") {
      &hex[1..]
    } else {
      hex
    };

    let r = u8::from_str_radix(&start[0..2], 16).unwrap();
    let g = u8::from_str_radix(&start[2..4], 16).unwrap();
    let b = u8::from_str_radix(&start[4..6], 16).unwrap();
    let a = if start.len() > 6 {
      u8::from_str_radix(&start[6..8], 16).unwrap()
    } else {
      255u8
    };

    Color {
      r, g, b, a,
    }
  }

  pub fn as_f32(&self) -> (f32, f32, f32, f32) {
    (self.r as f32 / 255.0, self.g as f32 / 255.0, self.b as f32 / 255.0, self.a as f32 / 255.0)
  }

  pub fn as_f64(&self) -> (f64, f64, f64, f64) {
    (self.r as f64 / 255.0, self.g as f64 / 255.0, self.b as f64 / 255.0, self.a as f64 / 255.0)
  }
}

pub mod bindings {
  use std::ffi::CString;

  use byte_strings::c_str;
  use libc::c_int;
  use libc::c_void;
  use libquickjs_sys::*;
  use crate::js::Class;
  use crate::js::ClassData;
  use crate::js::FuncDef;
  use crate::js::JSCFunctionDef;
  use crate::js::undefined;
  use crate::js::new_obj_helper;
  use crate::js::jsvalue_to_string;

  use super::*;

  pub static mut COLOR_ID: JSClassID = 0;
  pub static mut COLOR_PROTO: JSValue = undefined();

  pub fn get_classes(ctx: *mut JSContext) -> Vec<Class> {
    unsafe { 
      JS_NewClassID(&mut COLOR_ID);
      COLOR_PROTO = JS_NewObject(ctx);

      vec![
        Class::new(ClassData {
          id: COLOR_ID,
          proto: COLOR_PROTO,
          name: c_str!("Color"),
          ctor: js_color_ctor,
          ctor_args: 0,
          finalizer: Some(js_color_finalizer),
          entries: vec![
            JSCFunctionDef::magic_getset(c_str!("r"), js_color_get_rgba, js_color_set_rgba, 0),
            JSCFunctionDef::magic_getset(c_str!("g"), js_color_get_rgba, js_color_set_rgba, 1),
            JSCFunctionDef::magic_getset(c_str!("b"), js_color_get_rgba, js_color_set_rgba, 2),
            JSCFunctionDef::magic_getset(c_str!("a"), js_color_get_rgba, js_color_set_rgba, 3),
            JSCFunctionDef::func(c_str!("toString"), js_color_tostring, 0),
          ]
        }).with_statics(vec![
          FuncDef {
            name: c_str!("fromHex"),
            argc: 1,
            func: js_color_fromhex,
          }
        ]),
      ]
    }
  }

  unsafe extern "C" fn js_color_get_rgba(ctx: *mut JSContext, this_val: JSValue, magic: c_int) -> JSValue {
    let c: &mut Color = &mut *(JS_GetOpaque(this_val, COLOR_ID) as *mut Color);
    match magic {
      0 => JS_NewInt32(ctx, c.r as i32),
      1 => JS_NewInt32(ctx, c.g as i32),
      2 => JS_NewInt32(ctx, c.b as i32),
      _ => JS_NewInt32(ctx, c.a as i32),
    }
  }

  unsafe extern "C" fn js_color_set_rgba(ctx: *mut JSContext, this_val: JSValue, val: JSValue, magic: c_int) -> JSValue {
    let c: &mut Color = &mut *(JS_GetOpaque(this_val, COLOR_ID) as *mut Color);
    let mut i: i32 = 0;
    JS_ToInt32(ctx, &mut i, val);
    let byte = i as u8;
    match magic {
      0 => c.r = byte, 
      1 => c.g = byte, 
      2 => c.b = byte, 
      _ => c.a = byte, 
    };
    undefined()
  }

  unsafe extern "C" fn js_color_finalizer(_rt: *mut JSRuntime, value: JSValue) {
    Box::from_raw(JS_GetOpaque(value, COLOR_ID) as *mut Color);
  }

  unsafe extern "C" fn js_color_ctor(ctx: *mut JSContext, new_target: JSValue, argc: c_int, _argv: *mut JSValue) -> JSValue {
    if argc != 0 {
      return undefined();
    }

    let p = Box::new(Color::WHITE);
    let raw = Box::into_raw(p) as *mut c_void;
    new_obj_helper(ctx, new_target, COLOR_ID, raw)
  }

  unsafe extern "C" fn js_color_tostring(ctx: *mut JSContext, this: JSValue, _argc: c_int, _argv: *mut JSValue) -> JSValue {
    let vec = &mut *(JS_GetOpaque(this, COLOR_ID) as *mut Color);
    let s = format!("{:?}", vec);
    let cs = CString::new(s).unwrap();
    JS_NewString(ctx, cs.as_ptr())
  }

  unsafe extern "C" fn js_color_fromhex(ctx: *mut JSContext, _this: JSValue, _argc: c_int, argv: *mut JSValue) -> JSValue {
    // if argc != 1 {
    //   return undefined();
    // }

    let s = jsvalue_to_string(ctx, *argv);

    let p = Box::new(Color::from_hex(&s));
    let raw = Box::into_raw(p) as *mut c_void;
    let obj = JS_NewObjectProtoClass(ctx, COLOR_PROTO, COLOR_ID);

    JS_SetOpaque(obj, raw);

    obj
  }
}

#[cfg(test)]
mod tests {
  #[test]
  fn test_hex() {
    use super::Color;
    let white = Color::from_hex("#FFFFFF");
    let black = Color::from_hex("#000000");

    assert_eq!(Color::from_rgb(255, 255, 255), white);
    assert_eq!(Color::from_rgb(0, 0, 0), black);
  }
}