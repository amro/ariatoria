

use crate::{math::Rect, core::{Resources, World}};
use crate::gfx::{program::Program, render::{Command, Renderer, BufferId, BufferRef}};

const DEBUG_FRAG: &str = "res/shader/debug.frag";
const DEBUG_VERT: &str = "res/shader/debug.vert";

pub struct DebugBox(pub Rect<f32>);

#[derive(Clone, Copy, Debug)]
pub struct DebugVerts(pub [f32; 12]);

struct ProgramInput {
  program: u32,
  position_loc: u32,
  offset_loc: i32,
  ortho_loc: i32,
  color_loc: i32,
  vao: u32,
}

pub struct DebugSystem {
  buffer: BufferId,
  offset: usize,
  input: ProgramInput,
}

impl DebugSystem {
  pub fn new() -> Self {
    let vao = Renderer::named_vao("debug_vao");
    let program = Program::from_files("debug", DEBUG_VERT, DEBUG_FRAG).unwrap();
    let position_loc = program.get_attrib_location("position").unwrap();
    let offset_loc = program.get_uniform_location("offset").unwrap();
    let ortho_loc = program.get_uniform_location("ortho").unwrap();
    let color_loc = program.get_uniform_location("in_color").unwrap();

    let buffer = Renderer::named_buffer("debug_buffer", 2048*2048);
    DebugSystem {
      buffer,
      offset: 0,
      input: ProgramInput {
        program: program.id,
        position_loc,
        offset_loc: offset_loc as i32,
        ortho_loc: ortho_loc as i32,
        color_loc: color_loc as i32,
        vao,
      }
    }
  }
  pub fn debug_buffer_alloc(&mut self, world: &mut World) {
    for (entity, _debug_box) in world.components.debug_box.iter_mut() {
      if !world.components.debug_verts.has_component(entity) {
        world.components.debug_verts.insert(entity, DebugVerts([0.0; 12]));
        world.components.buffer_ref.insert(entity, BufferRef{
          id: self.buffer,
          offset: self.offset as isize
        });
        self.offset += 24;
      }
    }
  }

  pub fn debug_vert_gen(&self, resources: &mut Resources, world: &mut World) {
    let scale_factor = resources.scale_factor;
    for (entity, debug_box) in world.components.debug_box.iter() {
      let transform = world.components.transform.get(entity).unwrap();
      let mut verts = world.components.debug_verts.get_mut(entity).unwrap();
      let sf = scale_factor.0;
      let x = transform.position.x + debug_box.0.x;
      let y = transform.position.y + debug_box.0.y;
      let w = transform.size.x * sf * debug_box.0.width;
      let h = transform.size.y * sf * debug_box.0.height; 
      verts.0 = [
        x    , y    , 
        x    , y + h, 
        x + w, y + h, 
        x + w, y + h, 
        x + w, y    , 
        x    , y    , 
      ];
    }
  }

  pub fn upload_debug_verts(&self, world: &mut World) {
    for (entity, verts) in world.components.debug_verts.iter() {
      if let Some(buff) = world.components.buffer_ref.get(entity) {
        Renderer::render(&[Command::UploadFloats(buff.id, buff.offset, 12, &verts.0[0] as *const f32)]);
      }
    }
  }

  pub fn render(&self, resources: &mut Resources, world: &mut World) {
    let input = &self.input;
    let mut commands = Vec::new();
    for (entity, _verts) in world.components.debug_verts.iter() {
      if let Some(buff) = world.components.buffer_ref.get(entity) {
        commands.extend_from_slice(&[
          Command::UseProgram(input.program),
          Command::BindVertexArray(input.vao),
          Command::BindBuffer(buff.id),
          Command::EnableAttrib(input.position_loc as u32),
          Command::EnableWireframe,
        ]);


        commands.extend_from_slice(&[
          Command::VertexAttribPointer(input.position_loc as u32, 2, 8, buff.offset as usize),
          Command::SetUniform4(input.color_loc, 1.0, 1.0, 1.0, 1.0),
          Command::SetUniform2(input.offset_loc, 0.0, 0.0),
          Command::SetMatrix(input.ortho_loc as u32, resources.ortho_game.ptr()),
          Command::DrawArrays(0, 6),
        ]);

        commands.extend_from_slice(&[
          Command::DisableAttrib(input.position_loc as u32),
          Command::BindVertexArray(0),
          Command::DisableWireframe,
        ]);
      }
    }
    Renderer::render(&commands);
  }
}

