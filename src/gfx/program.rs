
use std::{ffi::CString, collections::HashMap};
use std::fs::File;

use std::path::Path;
use std::io::Read;

pub struct Program {
  pub id: u32,
}

fn check_shader_compile_status(id: u32) -> Option<String> {
  unsafe {
    let mut status: i32 = 0;
    gl::GetShaderiv(id, gl::COMPILE_STATUS, &mut status as *mut i32);
    if status as u8 == gl::FALSE {
      let mut length: i32 = 0;
      gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut length as *mut i32);

      let mut buffer: Vec<u8> = Vec::with_capacity(length as usize);
      buffer.set_len(length as usize);

      gl::GetShaderInfoLog(id, length, &mut length as *mut i32, buffer.as_mut_ptr() as *mut i8);

      return Some(String::from_utf8(buffer).unwrap());
    }
    None
  }
}

fn check_program_link_status(id: u32) -> Option<String> {
  unsafe {
    let mut status: i32 = 0;
    gl::GetProgramiv(id, gl::LINK_STATUS, &mut status as *mut i32);
    if status as u8 == gl::FALSE {
      let mut length: i32 = 0;
      gl::GetProgramiv(id, gl::INFO_LOG_LENGTH, &mut length as *mut i32);

      let mut buffer: Vec<u8> = Vec::with_capacity(length as usize);
      buffer.set_len(length as usize);

      gl::GetProgramInfoLog(id, length, &mut length as *mut i32, buffer.as_mut_ptr() as *mut i8);

      return Some(String::from_utf8(buffer).unwrap());
    }
    None
  }
}

fn read_to_buf(path: &str) -> Vec<u8> {
  let mut file = File::open(Path::new(path)).unwrap();
  let mut buf = Vec::new();
  file.read_to_end(&mut buf).unwrap();
  buf
}

impl Program {
  pub fn from_files(name: &str, vertex_shader_path: &str, fragment_shader_path: &str) -> Result<Self, String> {
    let vertex_shader = read_to_buf(vertex_shader_path);
    let fragment_shader = read_to_buf(fragment_shader_path);

    unsafe {
      let id: u32;

      let vertex_id: u32;
      let fragment_id: u32;
      vertex_id = gl::CreateShader(gl::VERTEX_SHADER);
      let mut cstr = CString::new(vertex_shader.clone()).unwrap();
      gl::ShaderSource(vertex_id, 1, &cstr.as_ptr() as *const *const i8, &(vertex_shader.len() as i32));
      gl::CompileShader(vertex_id);
      if let Some(err) = check_shader_compile_status(vertex_id) {
        return Err(err);
      }

      gl::ObjectLabel(gl::SHADER, vertex_id, vertex_shader_path.len() as i32, vertex_shader_path.as_ptr() as *const i8);

      fragment_id = gl::CreateShader(gl::FRAGMENT_SHADER);

      cstr = CString::new(fragment_shader.clone()).unwrap(); 
      gl::ShaderSource(fragment_id, 1, &cstr.as_ptr() as *const *const i8, &(fragment_shader.len() as i32));
      gl::CompileShader(fragment_id);
      if let Some(err) = check_shader_compile_status(fragment_id) {
        return Err(err);
      }


      gl::ObjectLabel(gl::SHADER, fragment_id, fragment_shader_path.len() as i32, fragment_shader_path.as_ptr() as *const i8);

      id = gl::CreateProgram();
      gl::AttachShader(id, vertex_id);
      gl::AttachShader(id, fragment_id);

      gl::LinkProgram(id);


      gl::ObjectLabel(gl::PROGRAM, id, name.len() as i32, name.as_ptr() as *const i8);

      if let Some(err) = check_program_link_status(id) {
        return Err(err);
      }

      Ok(Program {
        id,
      })

    }
  }

  pub fn get_attrib_location(&self, name: &str) -> Result<u32, ()> {
    let cstr = CString::new(name).unwrap();
    unsafe {
      let loc = gl::GetAttribLocation(self.id, cstr.as_ptr());
      if loc >= 0 {
        Ok(loc as u32)
      } else {
        Err(())
      }
    }
  }

  pub fn get_uniform_location(&self, name: &str) -> Result<i32, ()> {
    let cstr = CString::new(name).unwrap();
    unsafe {
      let loc = gl::GetUniformLocation(self.id, cstr.as_ptr());
      if loc >= 0 {
        Ok(loc)
      } else {
        Err(())
      }
    }
  }
}

pub struct Programs {
  programs: HashMap<String, Program>,
}

impl Programs {
  pub fn new() -> Self {
    Programs {
      programs: HashMap::new(),
    }
  }
}
