
use std::os::raw::c_void as void;
use std::mem::size_of;

use glutin::ContextWrapper;
use glutin::PossiblyCurrent;

#[derive(Debug, Clone, Copy)]
pub struct BufferId(pub u32);

#[derive(Clone, Copy, Debug)]
pub struct BufferRef {
  pub id: BufferId,
  pub offset: isize,
}

#[derive(Debug, Clone, Copy)]
pub struct MatrixPtr(pub *const f32);

unsafe impl Send for MatrixPtr { }
unsafe impl Sync for MatrixPtr { }

#[derive(Debug, Clone, Copy)]
pub enum Command {
  UseProgram(u32),
  BindVertexArray(u32),
  ActiveTexture(u32),
  BindTexture(u32),
  BindBuffer(BufferId),
  UploadFloats(BufferId, isize, isize, *const f32),
  DrawArrays(u32, u32),
  SetUniform1(i32, i32),
  SetUniform2(i32, f32, f32),
  SetUniform4(i32, f32, f32, f32, f32),
  SetMatrix(u32, *const f32),
  EnableAttrib(u32),
  DisableAttrib(u32),
  /// index, size, stride, pointer
  VertexAttribPointer(u32, i32, i32, usize),
  EnableWireframe,
  DisableWireframe,
}

pub struct Renderer {
}

impl Renderer {
  pub fn init<W>(gl_window: &mut ContextWrapper<PossiblyCurrent, W>) {
    // gl::ActiveTexture::load_with(|symbol| gl_window.get_proc_address(symbol) as *const _);
    // gl::AttachShader::load_with(|symbol| gl_window.get_proc_address(symbol) as *const _);
    // gl::BindBuffer::load_with(|symbol| gl_window.get_proc_address(symbol) as *const _);
    // gl::BindTexture::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::BindVertexArray::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::BlendFuncSeparate::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::BufferData::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::Clear::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::ClearColor::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::CompileShader::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::CreateProgram::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::CreateShader::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::DeleteTextures::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::DisableVertexAttribArray::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::DrawArrays::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::Enable::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::Disable::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::EnableVertexAttribArray::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::GenBuffers::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::GenTextures::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::GenVertexArrays::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::GetAttribLocation::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::GetProgramInfoLog::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::GetProgramiv::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::GetShaderInfoLog::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::GetShaderiv::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::GetUniformLocation::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::LinkProgram::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::NamedBufferSubData::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::ObjectLabel::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::PolygonMode::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::ShaderSource::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::TexImage2D::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::TexParameteri::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::TexSubImage2D::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::Uniform1i::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::Uniform2f::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::Uniform4f::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::UniformMatrix4fv::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::UseProgram::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::VertexAttribPointer::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    // gl::Viewport::load_with(|symbol| gl_window.get_proc_address(symbol) as * const _);
    gl::load_with(|name| gl_window.get_proc_address(name) as *const _);
    unsafe {
      gl::ClearColor(0.0, 0.0, 0.0, 1.0);
      gl::Enable(gl::BLEND);
      gl::BlendFuncSeparate(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA, gl::ONE, gl::ONE);
      gl::PolygonMode(gl::FRONT_AND_BACK, gl::FILL);
    }
  }

  pub fn resize(width: i32, height: i32) {
    unsafe {
      gl::Viewport(0, 0, width, height);
    }
  }

  pub fn new_vao() -> u32 {
    unsafe {
      let mut vao: u32 = 0;
      gl::GenVertexArrays(1, &mut vao as *mut u32);
      vao
    }
  }

  pub fn named_vao(name: &str) -> u32 {
    unsafe {
      let mut vao: u32 = 0;
      gl::GenVertexArrays(1, &mut vao as *mut u32);
      gl::ObjectLabel(gl::VERTEX_ARRAY, vao, name.len() as i32, name.as_ptr() as *const i8);
      vao
    }
  }

  pub fn active_texture() -> u32 {
    unsafe {
      let mut id: i32 = 0;
      gl::GetIntegerv(gl::TEXTURE_BINDING_2D, &mut id as *mut i32);
      id as u32
    }
  }

  pub fn named_buffer(name: &str, size: isize) -> BufferId {
    let mut id: u32 = 0;
    unsafe {
      gl::GenBuffers(1, &mut id as *mut u32);
      gl::BindBuffer(gl::ARRAY_BUFFER, id);
      gl::BufferData(gl::ARRAY_BUFFER, size, std::ptr::null(), gl::STREAM_DRAW);
      gl::ObjectLabel(gl::BUFFER, id, name.len() as i32, name.as_ptr() as *const i8);
    }

    BufferId(id)
  }

  pub fn render(commands: &[Command]) {
    for c in commands {
      match *c {
        Command::EnableWireframe => {
          unsafe {
            gl::PolygonMode(gl::FRONT_AND_BACK, gl::LINE);
          }
        },
        Command::DisableWireframe => {
          unsafe {
            gl::PolygonMode(gl::FRONT_AND_BACK, gl::FILL);
          }
        },
        Command::ActiveTexture(id) => {
          unsafe {
            gl::ActiveTexture(id);
          }
        },
        Command::UseProgram(id) => {
          unsafe {
            gl::UseProgram(id);
          }
        },
        Command::BindVertexArray(id) => {
          unsafe {
            gl::BindVertexArray(id);
          }
        },
        Command::BindTexture(id) => {
          unsafe {
            gl::ActiveTexture(gl::TEXTURE0);
            gl::BindTexture(gl::TEXTURE_2D, id);
          }
        },
        Command::BindBuffer(bid) => {
          unsafe {
            gl::BindBuffer(gl::ARRAY_BUFFER, bid.0);
          }
        },

        Command::DrawArrays(first, count) => {
          unsafe {
            gl::DrawArrays(gl::TRIANGLES, first as i32, count as i32);
          }
        },
        Command::SetMatrix(location, data) => {
          unsafe {
            gl::UniformMatrix4fv(location as i32, 1, gl::FALSE, data);
          }
        },
        Command::SetUniform1(location, data) => {
          unsafe {
            gl::Uniform1i(location, data);
          }
        },
        Command::SetUniform2(location, datax, datay) => {
          unsafe {
            gl::Uniform2f(location, datax, datay);
          }
        },
        Command::SetUniform4(location, x, y, z, w) => {
          unsafe {
            gl::Uniform4f(location, x, y, z, w);
          }
        },
        Command::EnableAttrib(location) => {
          unsafe {
            gl::EnableVertexAttribArray(location);
          }
        },
        Command::DisableAttrib(location) => {
          unsafe {
            gl::DisableVertexAttribArray(location);
          }
        },
        Command::VertexAttribPointer(index, size, stride, offset) => {
          let offset = offset * size_of::<f32>();
          unsafe {
            gl::VertexAttribPointer(index, size, gl::FLOAT, 0, stride, offset as *const void);
          }
        },
        Command::UploadFloats(bid, offset, size, data) => {
          unsafe {
            gl::NamedBufferSubData(bid.0, offset * size_of::<f32>() as isize, size * size_of::<f32>() as isize, data as *const void);
          }
        }
      }
    }
  }
}
