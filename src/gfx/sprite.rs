
use crate::gfx::render::BufferId;
use crate::gfx::render::BufferRef;
use crate::core::{Resources, World};
use crate::gfx::program::Program;
use crate::gfx::render::Command;
use crate::gfx::render::Renderer;

const TILE_FRAG: &str = "res/shader/tile.frag";
const TILE_VERT: &str = "res/shader/tile.vert";

pub struct Sprite {
  pub coords: (f32, f32, f32, f32),
  width: f32,
  height: f32,
  layer: u8,
}

impl Sprite {
  pub fn new(layer: u8, width: f32, height: f32, coords: (f32, f32, f32, f32)) -> Self {
    Sprite {
      width,
      height,
      layer,
      coords,
    }
  }
}

#[derive(Clone, Copy, Debug)]
pub struct SpriteVerts(pub [f32; 24]);

pub fn sprite_buffer_alloc(resources: &mut Resources, world: &mut World) {
  let mut buffer = resources.buffers.get_named("sprite");
  for (entity, _sprite) in world.components.sprite.iter() {
    if !world.components.sprite_verts.has_component(entity) {
      world.components.sprite_verts.insert(entity, SpriteVerts([0.0; 24]));
      world.components.buffer_ref.insert(entity, BufferRef {
        id: buffer.id,
        offset: buffer.offset as isize
      });

      buffer.offset += 24;
    }
  }
}

pub fn sprite_vert_gen(resources: &mut Resources, world: &mut World) {
  let scale_factor = resources.scale_factor.0;
  for (entity, sprite) in world.components.sprite.iter() {
    let transform = world.components.transform.get(entity).unwrap();
    let mut verts = world.components.sprite_verts.get_mut(entity).unwrap();
    let (u, v, us, vs) = sprite.coords;
    let sf = scale_factor;
    let x = transform.position.x;
    let y = transform.position.y;
    let w = transform.size.x * sf * sprite.width;
    let h = transform.size.y * sf * sprite.height; 
    verts.0 = [
      x    , y    , u     , v     ,
      x    , y + h, u     , v + vs,
      x + w, y + h, u + us, v + vs,
      x + w, y + h, u + us, v + vs,
      x + w, y    , u + us, v     ,
      x    , y    , u     , v
    ];
  }
}

pub fn upload_sprite_verts(world: &mut World) {
  for (entity, verts) in world.components.sprite_verts.iter() {
    if let Some(buff) = world.components.buffer_ref.get(entity) {
      Renderer::render(&[Command::UploadFloats(buff.id, buff.offset, 24, &verts.0[0] as *const f32)]);
    }
  }
}

struct ProgramInput {
  position_loc: u32,
  coords_loc: u32,
  texture_loc: i32,
  offset_loc: i32,
  ortho_loc: i32,
  color_loc: i32,
}

struct SpriteBuffer {
  id: BufferId,
  offset: usize,
}

pub struct SpriteSystem {
  input: ProgramInput,
  program: Program,
  vao: u32,
}

impl SpriteSystem {
  pub fn new() -> Self {
    let vao = Renderer::named_vao("sprite_vao");
    let program = Program::from_files("sprite", TILE_VERT, TILE_FRAG).unwrap();
    let position_loc = program.get_attrib_location("position").unwrap();
    let coords_loc = program.get_attrib_location("coords").unwrap();
    let texture_loc = program.get_uniform_location("texture").unwrap();
    let offset_loc = program.get_uniform_location("offset").unwrap();
    let ortho_loc = program.get_uniform_location("ortho").unwrap();
    let color_loc = program.get_uniform_location("in_color").unwrap();

    SpriteSystem {
      vao,
      program,
      input: ProgramInput {
        position_loc,
        coords_loc,
        texture_loc,
        offset_loc,
        ortho_loc,
        color_loc
      }
    }
  }

  pub fn render(&self, resources: &Resources, world: &World) {
    let mut layer_commands = vec![Vec::new(); 4];
    for (entity, sprite) in world.components.sprite.iter() {
      let texture = world.components.texture.get(entity).unwrap();
      let buff = world.components.buffer_ref.get(entity).unwrap();
      let tex_opt = resources.texture_system.get(*texture);
      if tex_opt.is_none() {
        continue;
      }
      let commands = &mut layer_commands[sprite.layer as usize];
      let tex = tex_opt.unwrap();

      commands.extend_from_slice(&[
        Command::UseProgram(self.program.id),
        Command::BindVertexArray(self.vao),
        Command::BindBuffer(buff.id),
        Command::EnableAttrib(self.input.position_loc as u32),
        Command::EnableAttrib(self.input.coords_loc as u32),
      ]);


      commands.extend_from_slice(&[
        Command::VertexAttribPointer(self.input.position_loc as u32, 2, 16, buff.offset as usize),
        Command::VertexAttribPointer(self.input.coords_loc as u32, 2, 16, (buff.offset + 2) as usize),
        Command::BindTexture(tex.id),
        Command::SetUniform4(self.input.color_loc, 1.0, 1.0, 1.0, 1.0),
        Command::SetUniform1(self.input.texture_loc, 0),
        Command::SetUniform2(self.input.offset_loc, 0.0, 0.0),
        Command::SetMatrix(self.input.ortho_loc as u32, resources.ortho_game.ptr()),
        Command::DrawArrays(0, 6),
      ]);

      commands.extend_from_slice(&[
        Command::DisableAttrib(self.input.position_loc as u32),
        Command::DisableAttrib(self.input.coords_loc as u32),
        Command::BindVertexArray(0),
      ]);
    }
          
    for layer in layer_commands {
      Renderer::render(&layer);
    }
  }
}
