
#[derive(Debug, Clone)]
pub struct Spritesheet {
  width: f32,
  height: f32,
  tile_width: f32,
  tile_height: f32,
  texture_width: f32,
  texture_height: f32,
}

#[derive(Clone, PartialEq, Eq, Hash)]
pub struct SpritesheetSpec {
  pub texture_width: u32,
  pub texture_height: u32,
  pub tile_width: u32,
  pub tile_height: u32,
}

impl Spritesheet {
  pub fn from_spec(spec: SpritesheetSpec) -> Self {
    let width = spec.texture_width / spec.tile_width;
    let height = spec.texture_height / spec.tile_height;
    Spritesheet {
      width: width as f32,
      height: height as f32,
      texture_width: spec.texture_width as f32,
      texture_height: spec.texture_height as f32,
      tile_width: spec.tile_width as f32,
      tile_height: spec.tile_height as f32
    }
  }

  pub fn get_coords(&self, idx: u32) -> (f32, f32, f32, f32) {
    let y = (idx / self.width as u32) as f32;
    let x = (idx as f32) - (y * self.width) as f32;
    let y = self.height - y - 1.0;
    ((x * self.tile_width) / self.texture_width, (y * self.tile_height) / self.texture_height, self.tile_width / self.texture_width, self.tile_height / self.texture_height)
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn get_coords() {
    let s = Spritesheet::from_spec(SpritesheetSpec {
      texture_width: 256,
      texture_height: 256,
      tile_width: 16,
      tile_height: 16
    });

    assert_eq!((0.0, 0.9375, 0.0625, 0.0625), s.get_coords(0));
    assert_eq!((0.0625, 0.9375, 0.0625, 0.0625), s.get_coords(1));
    assert_eq!((0.0, 0.875, 0.0625, 0.0625), s.get_coords(16));
  }
}
