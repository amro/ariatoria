use crate::core::asset;

use crate::{generational_index::Index, math::Rect};

use log::error;
use std::{collections::HashMap, os::raw::c_void as void, sync::{Mutex, Arc}};
use stb_image::image::LoadResult;
use asset::{Handle, Assets};

use crate::gfx::Color;

#[derive(Debug)]
pub struct Texture {
  pub id: u32,
  pub width: u32,
  pub height: u32
}

impl Texture {
  pub fn null() -> Self {
    Texture {
      id: 0,
      width: 0,
      height: 0,
    }
  }

  pub fn from_png(file: &str) -> Result<Self, asset::Error> {
    let mut id: u32 = 0;
    let img = match stb_image::image::load(file) {
      LoadResult::Error(e) => {
        error!("Error loading image {}", e);
        return Err(asset::Error::InvalidFormat);
      },
      LoadResult::ImageU8(img) => img,
      LoadResult::ImageF32(_) => {
        error!("Unsupported HDR png {}", file);
        return Err(asset::Error::InvalidFormat);
      }
    };
    let mut buf_rev = Vec::with_capacity(img.data.len());
    let width = img.width as usize;
    let height = img.height as usize;
    for y in (0..height).rev() {
      for x in 0..width {
        buf_rev.push(img.data[(y*width*4) + (x*4)]);
        buf_rev.push(img.data[(y*width*4) + (x*4) + 1]);
        buf_rev.push(img.data[(y*width*4) + (x*4) + 2]);
        buf_rev.push(img.data[(y*width*4) + (x*4) + 3]);
      }
    }
    unsafe {
      gl::GenTextures(1, &mut id as *mut u32);

      gl::BindTexture(gl::TEXTURE_2D, id);
      gl::ObjectLabel(gl::TEXTURE, id, file.len() as i32, file.as_ptr() as *const i8);
      gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE as i32);
      gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE as i32);
      gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as i32);
      gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as i32);

      gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGBA as i32, img.width as i32, img.height as i32, 0, gl::RGBA, gl::UNSIGNED_BYTE, buf_rev.as_ptr() as *const void);
    }
    Ok(Texture {
      id,
      width: img.width as u32,
      height: img.height as u32,
    })
  }

  pub fn with_size(name: &str, width: u32, height: u32) -> Self {
    let mut id: u32 = 0;
    unsafe {
      gl::GenTextures(1, &mut id as *mut u32);
      gl::BindTexture(gl::TEXTURE_2D, id);
      gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE as i32);
      gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE as i32);
      gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32);
      gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);

      gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGBA as i32, width as i32, height as i32, 0, gl::RGBA, gl::UNSIGNED_BYTE, std::ptr::null());
      gl::ObjectLabel(gl::TEXTURE, id, name.len() as i32, name.as_ptr() as *const i8);
    }
    Texture {
      id,
      width,
      height,
    }
  }

  pub fn _clear(&mut self) {
    unsafe {
      gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGBA as i32, self.width as i32, self.height as i32, 0, gl::RGBA, gl::UNSIGNED_BYTE, std::ptr::null());
    }
  }

  pub fn upload(id: u32, r: Rect<u32>, data: &[Color]) {
    unsafe {
      gl::BindTexture(gl::TEXTURE_2D, id);
      gl::TexSubImage2D(gl::TEXTURE_2D, 0, r.x as i32, r.y as i32, r.width as i32, r.height as i32, gl::RGBA, gl::UNSIGNED_BYTE, data.as_ptr() as *const void)
    }
  }
}

impl Drop for Texture {
  fn drop(&mut self) {
    unsafe {
      gl::DeleteTextures(1, &mut self.id as *mut u32);
    }
  }
}

pub enum TextureAction {
  Create(String),
  CreateDynamic(u32, u32),
  Update(Rect<u32>, Vec<Color>)
}

#[derive(Clone)]
pub struct TextureQueue(Arc<Mutex<Vec<(Index, TextureAction)>>>);

impl TextureQueue {
  pub fn push(&self, index: Index, action: TextureAction) {
    self.0.lock().unwrap().push((index, action));
  }
}

pub struct TextureSystem {
  lookup:  HashMap<String, Handle<Texture>>,
  textures: Assets<Texture>,
}

impl TextureSystem {
  pub fn new() -> Self {
    TextureSystem {
      lookup: HashMap::new(),
      textures: Assets::new(),
    }
  }

  pub fn load(&mut self, spec: &str) -> Handle<Texture> {
    if let Some(handle) = self.lookup.get(spec) {
      return *handle;
    }
    let handle = self.textures.allocate();
    let tex = Texture::from_png(&spec).unwrap();
    self.textures.put(handle, tex);
    self.lookup.insert(spec.into(), handle);
    handle
  }
  
  pub fn get(&self, handle: Handle<Texture>) -> Option<&Texture> {
    self.textures.get(handle)
  }

  pub fn allocate(&mut self, name: &str, width: u32, height: u32) -> Handle<Texture> {
    let handle = self.textures.allocate();
    let tex = Texture::with_size(name, width, height);
    self.textures.put(handle, tex);
    handle
  }

  pub fn update(&mut self, handle: Handle<Texture>, x: u32, y: u32, width: u32, height: u32, bitmap: Vec<Color>) {
    let tex = self.textures.get(handle).unwrap();
    Texture::upload(tex.id, Rect { x, y, width, height }, &bitmap);
  }
}


