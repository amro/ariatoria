
use std::mem;

use font_kit::font::Font;
use pathfinder_color::ColorU;
use pathfinder_content::fill::FillRule;
use pathfinder_content::outline::Contour;
use pathfinder_content::outline::Outline;
use pathfinder_content::stroke::OutlineStrokeToFill;
use pathfinder_content::stroke::StrokeStyle;
use pathfinder_geometry::rect::RectF;
use pathfinder_geometry::rect::RectI;
use pathfinder_geometry::transform2d::Transform2F;
use pathfinder_geometry::vector::Vector2F;
use pathfinder_geometry::vector::Vector2I;
use pathfinder_geometry::vector::vec2i;
use pathfinder_renderer::paint::Paint;
use pathfinder_renderer::scene::DrawPath;
use pathfinder_renderer::scene::Scene;
use skribo::FontCollection;
use skribo::FontFamily;
use skribo::TextStyle;

use crate::gfx::Color;
use crate::gfx::ui::Theme;

use super::text::FontContext;
use super::text::FontRenderOptions;


#[derive(Clone)]
pub struct Path2D {
    outline: Outline,
    current_contour: Contour,
}

impl Path2D {
  #[inline]
  pub fn new() -> Path2D {
    Path2D { outline: Outline::new(), current_contour: Contour::new() }
  }

  #[inline]
  pub fn close_path(&mut self) {
    self.current_contour.close();
  }

  #[inline]
  pub fn move_to(&mut self, to: Vector2F) {
    self.flush_current_contour();
    self.current_contour.push_endpoint(to);
  }

  #[inline]
  pub fn line_to(&mut self, to: Vector2F) {
    self.current_contour.push_endpoint(to);
  }

  #[inline]
  pub fn quadratic_curve_to(&mut self, ctrl: Vector2F, to: Vector2F) {
    self.current_contour.push_quadratic(ctrl, to);
  }

  #[inline]
  pub fn bezier_curve_to(&mut self, ctrl0: Vector2F, ctrl1: Vector2F, to: Vector2F) {
    self.current_contour.push_cubic(ctrl0, ctrl1, to);
  }

  pub fn rect(&mut self, rect: RectF) {
    self.flush_current_contour();
    self.current_contour.push_endpoint(rect.origin());
    self.current_contour.push_endpoint(rect.upper_right());
    self.current_contour.push_endpoint(rect.lower_right());
    self.current_contour.push_endpoint(rect.lower_left());
    self.current_contour.close();
  }

  pub fn into_outline(mut self) -> Outline {
    self.flush_current_contour();
    self.outline
  }

  fn flush_current_contour(&mut self) {
    if !self.current_contour.is_empty() {
      self.outline.push_contour(mem::replace(&mut self.current_contour, Contour::new()));
    }
  }
}


struct State {
  paint: Paint,
  transform_needs_update: bool,
  transform_current: Transform2F,
  transforms: Vec<Transform2F>,
}

pub struct Canvas {
  scene: Scene,
  collection: FontCollection,
  fc: FontContext<font_kit::loaders::default::Font>,
  state: State,
  theme: Theme,
  size: Vector2F,
}

impl State {
  pub fn new() -> Self {
    State {
      paint: Paint::transparent_black(),
      transforms: Vec::new(),
      transform_needs_update: false,
      transform_current: Transform2F::default(),
    }
  }
}

impl Canvas {
  pub fn new(size: Vector2F) -> Self {
    let mut scene = Scene::new();
    let vbox = RectI::new(Vector2I::default(), vec2i(size.x() as i32, size.y() as i32)).to_f32();
    scene.set_bounds(vbox);
    scene.set_view_box(vbox);
    let state = State::new();
    let fc = FontContext::new();
    let mut collection = FontCollection::new();

    collection.add_family(FontFamily::new_from_font(Font::from_path("res/fonts/code.ttf", 0).unwrap()));

    Canvas {
      scene,
      state,
      size,
      theme: Theme::default(),
      collection,
      fc,
    }
  }

  pub fn theme(&self) -> &Theme {
    &self.theme
  }

  pub fn current_transform(&mut self) -> &Transform2F {
    if self.state.transform_needs_update {
      let mut t = Transform2F::default();
      for transform in &self.state.transforms {
        t = t * *transform;
      }
      self.state.transform_current = t;
      self.state.transform_needs_update = false;
    }
    &self.state.transform_current
  }

  pub fn push_translation(&mut self, translation: Vector2F) {
    self.push_transform(Transform2F::from_translation(translation));
  }

  pub fn push_transform(&mut self, transform: Transform2F) {
    self.state.transform_needs_update = true;
    self.state.transforms.push(transform);
  }

  pub fn pop_transform(&mut self) -> Option<Transform2F> {
    self.state.transform_needs_update = true;
    self.state.transforms.pop()
  }

  pub fn take_scene(&mut self) -> Scene {
    let scene = mem::replace(&mut self.scene, Scene::new());
    let vbox = RectI::new(Vector2I::default(), vec2i(self.size.x() as i32, self.size.y() as i32)).to_f32();
    self.scene.set_bounds(vbox);
    self.scene.set_view_box(vbox);
    scene
  }

  pub fn text(&mut self, text: &str) {
    let style = TextStyle { size: 14.0 };
    let options = FontRenderOptions {
      transform: *self.current_transform(),
      paint_id: self.scene.push_paint(&self.state.paint),
      ..Default::default()
    };
    self.fc.push_text(&mut self.scene, text, &style, &self.collection, &options).unwrap();
  }

  pub fn set_size(&mut self, size: Vector2F) {
    self.size = size;
    let vbox = RectF::new(Vector2F::default(), size);
    self.scene.set_bounds(vbox);
    self.scene.set_view_box(vbox);
  }

  pub fn source_color(&mut self, color: Color) {
    self.state.paint = Paint::from_color(ColorU::new(color.r, color.g, color.b, color.a));
  }

  fn push_path(&mut self, mut outline: Outline) {
    let paint_id = self.scene.push_paint(&self.state.paint);
    outline.transform(self.current_transform());
    let mut path = DrawPath::new(outline, paint_id);
    // path.set_clip_path(clip_path);
    path.set_fill_rule(FillRule::Winding);
    // path.set_blend_mode(blend_mode);
    self.scene.push_path(path);
  }

  pub fn fill_rect(&mut self, rect: RectF) {
    let mut path = Path2D::new();
    path.rect(rect);
    self.push_path(path.into_outline());
  }

  pub fn stroke_rect(&mut self, rect: RectF) {
    let mut path = Path2D::new();
    path.rect(rect);
    let outline = path.into_outline();
    let stroke_style = StrokeStyle {
      ..Default::default()
    };
    let mut stroke_to_fill = OutlineStrokeToFill::new(&outline, stroke_style);
    stroke_to_fill.offset();
    self.push_path(stroke_to_fill.into_outline());
  }
}

pub mod bindings {
  use byte_strings::c_str;
  use libc::c_int;
  use libc::c_void;
  use libquickjs_sys::*;
  use pathfinder_geometry::vector::vec2f;

  use crate::gfx::color::bindings::COLOR_ID;
  use crate::js::Class;
  use crate::js::ClassData;
  use crate::js::JSCFunctionDef;
  use crate::js::jsvalue_to_string;
  use crate::js::new_obj_helper;
  use crate::math::bindings::RECT_ID;
  use crate::math::bindings::VEC2F_ID;
  use crate::js::undefined;

  use super::*;

  pub static mut CANVAS_ID: JSClassID = 0;
  pub static mut CANVAS_PROTO: JSValue = undefined();

  pub fn get_classes(ctx: *mut JSContext) -> Vec<Class> {
    unsafe { 
      JS_NewClassID(&mut CANVAS_ID);
      CANVAS_PROTO = JS_NewObject(ctx);

      vec![
        Class::new(ClassData {
          id: CANVAS_ID,
          proto: CANVAS_PROTO,
          name: c_str!("Canvas"),
          ctor: js_canvas_ctor,
          ctor_args: 2,
          finalizer: Some(js_canvas_finalizer),
          entries: vec![
            JSCFunctionDef::func(c_str!("fillRect"), js_canvas_fill_rect, 0),
            JSCFunctionDef::func(c_str!("strokeRect"), js_canvas_stroke_rect, 0),
            JSCFunctionDef::func(c_str!("color"), js_canvas_color, 0),
            JSCFunctionDef::func(c_str!("resize"), js_canvas_resize, 0),
            JSCFunctionDef::func(c_str!("text"), js_canvas_text, 0),
            JSCFunctionDef::func(c_str!("pushTranslation"), js_canvas_push_translation, 0),
            JSCFunctionDef::func(c_str!("popTransform"), js_canvas_pop_transform, 0),
          ]
        }),
      ]
    }
  }

  unsafe extern "C" fn js_canvas_finalizer(_rt: *mut JSRuntime, value: JSValue) {
    Box::from_raw(JS_GetOpaque(value, CANVAS_ID) as *mut Canvas);
  }

  unsafe extern "C" fn js_canvas_fill_rect(_ctx: *mut JSContext, this: JSValue, _argc: c_int, argv: *mut JSValue) -> JSValue {
    let canvas = &mut *(JS_GetOpaque(this, CANVAS_ID) as *mut Canvas);
    let rect = *(JS_GetOpaque(*argv, RECT_ID) as *mut RectF).clone();
    canvas.fill_rect(rect);
    undefined()
  }

  unsafe extern "C" fn js_canvas_stroke_rect(_ctx: *mut JSContext, this: JSValue, _argc: c_int, argv: *mut JSValue) -> JSValue {
    let canvas = &mut *(JS_GetOpaque(this, CANVAS_ID) as *mut Canvas);
    let rect = *(JS_GetOpaque(*argv, RECT_ID) as *mut RectF).clone();
    canvas.stroke_rect(rect);
    undefined()
  }

  unsafe extern "C" fn js_canvas_color(_ctx: *mut JSContext, this: JSValue, _argc: c_int, argv: *mut JSValue) -> JSValue {
    let canvas = &mut *(JS_GetOpaque(this, CANVAS_ID) as *mut Canvas);
    let color = *(JS_GetOpaque(*argv, COLOR_ID) as *mut Color).clone();
    canvas.source_color(color);
    undefined()
  }

  unsafe extern "C" fn js_canvas_resize(ctx: *mut JSContext, this: JSValue, _argc: c_int, argv: *mut JSValue) -> JSValue {
    let canvas = &mut *(JS_GetOpaque(this, CANVAS_ID) as *mut Canvas);

    let (mut x, mut y) = (0.0, 0.0);
    JS_ToFloat64(ctx, &mut x, *argv);
    JS_ToFloat64(ctx, &mut y, *(argv.add(1)));
    canvas.set_size(vec2f(x as f32, y as f32));
    undefined()
  }

  unsafe extern "C" fn js_canvas_text(ctx: *mut JSContext, this: JSValue, _argc: c_int, argv: *mut JSValue) -> JSValue {
    let canvas = &mut *(JS_GetOpaque(this, CANVAS_ID) as *mut Canvas);

    let s = jsvalue_to_string(ctx, *argv);
    canvas.text(&s);
    undefined()
  }

  unsafe extern "C" fn js_canvas_push_translation(_ctx: *mut JSContext, this: JSValue, _argc: c_int, argv: *mut JSValue) -> JSValue {
    let canvas = &mut *(JS_GetOpaque(this, CANVAS_ID) as *mut Canvas);
    let vec = *(JS_GetOpaque(*argv, VEC2F_ID) as *mut Vector2F).clone();

    canvas.push_translation(vec);

    undefined()
  }

  unsafe extern "C" fn js_canvas_pop_transform(_ctx: *mut JSContext, this: JSValue, _argc: c_int, _argv: *mut JSValue) -> JSValue {
    let canvas = &mut *(JS_GetOpaque(this, CANVAS_ID) as *mut Canvas);

    canvas.pop_transform();
    undefined()
  }

  unsafe extern "C" fn js_canvas_ctor(ctx: *mut JSContext, new_target: JSValue, argc: c_int, argv: *mut JSValue) -> JSValue {
    if argc != 1 {
      return undefined();
    }

    let vec = *(JS_GetOpaque(*argv, VEC2F_ID) as *mut Vector2F).clone();
    let c = Box::new(Canvas::new(vec));

    let raw = Box::into_raw(c) as *mut c_void;

    new_obj_helper(ctx, new_target, CANVAS_ID, raw)
  }

}