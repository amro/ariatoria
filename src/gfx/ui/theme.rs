use crate::gfx::Color;

pub struct Theme {
  pub bg: Color,
  pub deep_bg: Color,
  pub button_bg: Color,
  pub button_bg_disabled: Color,
  pub button_border: Color,
  pub tab_border: Color,
}

impl Theme {
  pub fn default() -> Self {
    Theme {
      bg: Color::from_hex("#333B4F"),
      deep_bg: Color::from_hex("#202531"),
      button_bg: Color::from_hex("#262C3B"),
      button_bg_disabled: Color::from_hex("#2E3547"),
      button_border: Color::from_hex("#202531"),
      tab_border: Color::from_hex("#191D28"),
    }
  }
}
