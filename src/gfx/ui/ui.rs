
use byte_strings::c_str;
use glutin::event::MouseButton;
use libc::c_void;
use pathfinder_geometry::vector::Vector2F;
use pathfinder_gl::GLDevice;
use pathfinder_gl::GLVersion;
use pathfinder_renderer::concurrent::rayon::RayonExecutor;
use pathfinder_renderer::scene::Scene;

use crate::gfx::program::Program;
use crate::gfx::render::BufferId;
use crate::gfx::render::Command;
use crate::gfx::render::Renderer;
use crate::gfx::ui::Canvas;
use crate::math::Matrix;

use pathfinder_gpu::Device;
use pathfinder_gpu::TextureFormat;
use pathfinder_resources::embedded::EmbeddedResourceLoader;
use pathfinder_renderer::concurrent::scene_proxy::SceneProxy;
use pathfinder_renderer::gpu::renderer::Renderer as PathfinderRenderer;
use pathfinder_renderer::gpu::options::{DestFramebuffer, RendererOptions};
use pathfinder_renderer::options::BuildOptions;
use pathfinder_color::ColorF;

const TILE_FRAG: &str = "res/shader/tile.frag";
const TILE_VERT: &str = "res/shader/tile.vert";

pub struct UI {
  buffer: BufferId,
  input: ProgramInput,
  program: Program,
  proxy: SceneProxy,
  renderer: PathfinderRenderer<GLDevice>,
  size: Vector2F,
  texture: u32,
  vao: u32,
}

impl std::fmt::Debug for UI {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    writeln!(f, "UI {{")?;
    writeln!(f, "  buffer: {}", self.buffer.0)?;
    writeln!(f, "  texture: {:?}", self.texture)?;
    writeln!(f, "  vao: {:?}", self.vao)?;
    writeln!(f, "}}")
  }
}

#[derive(Clone, Copy, Debug)]
pub struct UiVerts(pub [f32; 24]);

fn make_renderer(size: Vector2F) -> (u32, PathfinderRenderer<GLDevice>) {
  let device = GLDevice::new(GLVersion::GL3, 0);
  let texture = device.create_texture(TextureFormat::RGBA8, size.to_i32());
  let tex = Renderer::active_texture();
  let framebuffer = device.create_framebuffer(texture);
  let dest_fb = DestFramebuffer::Other(framebuffer);
  // let mode = RendererMode::default_for_device(&device);
  let options = RendererOptions {
      background_color: Some(ColorF::transparent_black()),
      // dest: DestFramebuffer::Other(framebuffer),
      ..RendererOptions::default()
  };

  let renderer = PathfinderRenderer::new(device, &EmbeddedResourceLoader, dest_fb, options);
  unsafe { gl::BindFramebuffer(gl::FRAMEBUFFER, 0); }
  (tex, renderer)
}

fn button_to_i32(button: MouseButton) -> i32 {
  match button {
    MouseButton::Left => 0,
    MouseButton::Right => 1,
    MouseButton::Middle => 2,
    MouseButton::Other(n) => n as i32,
  }
}

impl UI {
  pub fn new(size: Vector2F) -> Self {
    let vao = Renderer::named_vao("ui_vao");
    let program = Program::from_files("ui_program", TILE_VERT, TILE_FRAG).unwrap();
    let position_loc = program.get_attrib_location("position").unwrap();
    let coords_loc = program.get_attrib_location("coords").unwrap();
    let texture_loc = program.get_uniform_location("texture").unwrap();
    let offset_loc = program.get_uniform_location("offset").unwrap();
    let ortho_loc = program.get_uniform_location("ortho").unwrap();
    let color_loc = program.get_uniform_location("in_color").unwrap();
    let buffer = Renderer::named_buffer("ui_buffer", 512);

    let (texture, renderer) = make_renderer(size);


    UI {
      buffer,
      program,
      proxy: SceneProxy::new(RayonExecutor),
      renderer,
      size,
      texture,
      vao,
      input: ProgramInput {
        position_loc,
        coords_loc,
        texture_loc,
        offset_loc,
        ortho_loc,
        color_loc,
      }
    }
  }

  fn paint_from(&mut self, canvas: &mut Canvas) {
    self.paint_scene(canvas.take_scene());
  }

  fn paint_scene(&mut self, scene: Scene) {
    self.proxy.replace_scene(scene);
    self.proxy.build_and_render(&mut self.renderer, BuildOptions::default());
    unsafe {
      gl::BindFramebuffer(gl::FRAMEBUFFER, 0);
      gl::Viewport(0, 0, self.size.x() as i32, self.size.y() as i32);
      gl::Enable(gl::BLEND);
      gl::ClearColor(0.0, 0.0, 0.0, 1.0);
      gl::BlendFuncSeparate(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA, gl::ONE, gl::ONE);
    }
  }

  pub fn paint(&mut self) {
  }

  pub fn resize(&mut self, size: Vector2F) {
    let (texture, renderer) = make_renderer(size);
    self.texture = texture;
    self.renderer = renderer;
    self.size = size;
    self.vert_gen();
  }

  pub fn render(&self, projection: &Matrix) {
    let mut commands = Vec::new();
    commands.extend_from_slice(&[
      Command::UseProgram(self.program.id),
      Command::BindVertexArray(self.vao),
      Command::BindBuffer(self.buffer),
      Command::EnableAttrib(self.input.position_loc as u32),
      Command::EnableAttrib(self.input.coords_loc as u32),
    ]);

    commands.extend_from_slice(&[
      Command::VertexAttribPointer(self.input.position_loc as u32, 2, 16, 0),
      Command::VertexAttribPointer(self.input.coords_loc as u32, 2, 16, 2),
      Command::BindTexture(self.texture),
      Command::SetUniform4(self.input.color_loc, 1.0, 1.0, 1.0, 1.0),
      Command::SetUniform1(self.input.texture_loc, 0),
      Command::SetUniform2(self.input.offset_loc, 0.0, 0.0),
      Command::SetMatrix(self.input.ortho_loc as u32, projection.ptr()),
      Command::DrawArrays(0, 6),
    ]);

    commands.extend_from_slice(&[
      Command::DisableAttrib(self.input.position_loc as u32),
      Command::DisableAttrib(self.input.coords_loc as u32),
      Command::BindVertexArray(0),
    ]);
        
    Renderer::render(&commands);
  }

  pub fn vert_gen(&mut self) {
    let sf = 1.0;
    let (u, v, us, vs) = (0.0, 0.0, 1.0, 1.0);
    let x = 0.0;
    let y = 0.0;
    let w = self.size.x() * sf;
    let h = self.size.y() * sf; 
    let verts = [
      x    , y    , u     , v     ,
      x    , y + h, u     , v + vs,
      x + w, y + h, u + us, v + vs,
      x + w, y + h, u + us, v + vs,
      x + w, y    , u + us, v     ,
      x    , y    , u     , v
    ];
    Renderer::render(&[Command::UploadFloats(self.buffer, 0, 24, &verts[0] as *const f32)]);
  }
}

struct ProgramInput {
  position_loc: u32,
  coords_loc: u32,
  texture_loc: i32,
  offset_loc: i32,
  ortho_loc: i32,
  color_loc: i32,
}

struct UiBuffer {
  id: BufferId,
  offset: usize,
}

pub mod bindings {
  use libc::c_int;
  use libquickjs_sys::*;
  use pathfinder_geometry::vector::vec2f;
  use super::*;

  use crate::gfx::ui::canvas::bindings::CANVAS_ID;
  use crate::js::Class;
  use crate::js::ClassData;
  use crate::js::JSCFunctionDef;
  use crate::js::new_obj_helper;
  use crate::math::bindings::MATRIX_ID;
  use crate::math::bindings::VEC2F_ID;
  use crate::js::undefined;

  pub static mut UI_ID: JSClassID = 0;
  pub static mut UI_PROTO: JSValue = undefined();

  pub fn get_classes(ctx: *mut JSContext) -> Vec<Class> {
    unsafe { 
      JS_NewClassID(&mut UI_ID);
      UI_PROTO = JS_NewObject(ctx);

      vec![
        Class::new(ClassData {
          id: UI_ID,
          proto: UI_PROTO,
          name: c_str!("UI"),
          ctor: js_ui_ctor,
          ctor_args: 2,
          finalizer: Some(js_ui_finalizer),
          entries: vec![
            JSCFunctionDef::func(c_str!("paint"), js_ui_paint, 1),
            JSCFunctionDef::func(c_str!("resize"), js_ui_resize, 2),
            JSCFunctionDef::func(c_str!("render"), js_ui_render, 1),
          ]
        }),
      ]
    }
  }

  pub unsafe extern "C" fn js_ui_paint(_ctx: *mut JSContext, this: JSValue, argc: c_int, argv: *mut JSValue) -> JSValue {
    let ui = &mut *(JS_GetOpaque(this, UI_ID) as *mut UI);
    let canvas = &mut *(JS_GetOpaque(*argv, CANVAS_ID) as *mut Canvas);
    if argc != 1 {
      panic!();
    }

    ui.paint_from(canvas);
    undefined()
  }

  unsafe extern "C" fn js_ui_resize(ctx: *mut JSContext, this: JSValue, _argc: c_int, argv: *mut JSValue) -> JSValue {
    let ui = &mut *(JS_GetOpaque(this, UI_ID) as *mut UI);

    let (mut x, mut y) = (0.0, 0.0);
    JS_ToFloat64(ctx, &mut x, *argv);
    JS_ToFloat64(ctx, &mut y, *(argv.add(1)));
    ui.resize(vec2f(x as f32, y as f32));
    undefined()
  }

  unsafe extern "C" fn js_ui_render(_ctx: *mut JSContext, this: JSValue, _argc: c_int, argv: *mut JSValue) -> JSValue {
    let ui = &mut *(JS_GetOpaque(this, UI_ID) as *mut UI);
    let matrix = *(JS_GetOpaque(*argv, MATRIX_ID) as *mut Matrix);

    ui.render(&matrix);
    undefined()
  }

  unsafe extern "C" fn js_ui_ctor(ctx: *mut JSContext, new_target: JSValue, argc: c_int, argv: *mut JSValue) -> JSValue {
    if argc != 1 {
      return undefined();
    }

    let vec = *(JS_GetOpaque(*argv, VEC2F_ID) as *mut Vector2F).clone();
    let c = Box::new(UI::new(vec));

    let raw = Box::into_raw(c) as *mut c_void;

    new_obj_helper(ctx, new_target, UI_ID, raw)
  }

  unsafe extern "C" fn js_ui_finalizer(_rt: *mut JSRuntime, value: JSValue) {
    Box::from_raw(JS_GetOpaque(value, UI_ID) as *mut UI);
  }

}
