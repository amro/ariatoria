use std::ffi::CStr;
use std::ffi::CString;
use std::fs::File;
use std::io::Read;
use byte_strings::c_str;
use libc::c_int;
use libc::c_void;
use libquickjs_sys::*;

mod module;
mod value;

pub use module::Module;
pub use module::JSCFunctionDef;
pub use module::Class;
pub use module::ClassData;


use crate::js::module::Func;
pub use crate::js::module::FuncDef;
pub use crate::js::value::jsvalue_to_string;
pub use crate::js::value::ValueRef;
pub use crate::js::value::undefined;

pub type FuncData        = unsafe extern "C" fn(*mut JSContext, JSValue, c_int, *mut JSValue, c_int, *mut JSValue) -> JSValue;

pub struct Runtime {
  runtime: *mut JSRuntime,
  context: *mut JSContext,
}

pub unsafe fn new_obj_helper(ctx: *mut JSContext, new_target: JSValue, id: JSClassID, opaque: *mut c_void) -> JSValue {
  let proto = JS_GetPropertyStr(ctx, new_target, c_str!("prototype").as_ptr());
  let obj = JS_NewObjectProtoClass(ctx, proto, id);
  JS_FreeValue(ctx, proto);
  JS_SetOpaque(obj, opaque);
  obj
}

pub unsafe fn new_obj_proto<T>(ctx: *mut JSContext, proto: JSValue, id: JSClassID, heap: Box<T>) -> JSValue {
  let opaque = Box::leak(heap) as *mut T as *mut c_void;
  let obj = JS_NewObjectProtoClass(ctx, proto, id);
  JS_SetOpaque(obj, opaque);
  obj
}

unsafe extern "C" fn loggerfn(ctx: *mut JSContext, _this: JSValue, argc: c_int, argv: *mut JSValue, _magic: c_int, _data: *mut JSValue) -> JSValue {
  for i in 0..argc {
    let s = jsvalue_to_string(ctx, *argv.add(i as usize));
    print!("{}", s);
  }
  println!("");
  JSValue {
    u: JSValueUnion { int32: 0 },
    tag: JS_TAG_NULL as i64,
  }
}

impl Runtime {
  pub fn new() -> Self {
    let runtime = unsafe { JS_NewRuntime() };
    if runtime.is_null() {
      panic!();
    }

    let context = unsafe { JS_NewContext(runtime) };
    if context.is_null() {
      unsafe {
        JS_FreeRuntime(runtime);
      }
      panic!();
    }

    let null = std::ptr::null_mut();

    unsafe {
      JS_SetModuleLoaderFunc(runtime, None, Some(module::module_loader), null);
    }

    let mut rt = Runtime {
      runtime,
      context,
    };

    unsafe {
      rt.bind_function_data(c_str!("log"), loggerfn, -1, 0, 20 as *mut c_void);
    }

    rt
  }

  pub unsafe fn register_module(&mut self, name: &'static CStr, classes: Vec<Class>) {
    module::REGISTRY.register(name, Module {
      name,
      classes,
    });
  }
  
  pub fn get_context(&self) -> *mut JSContext {
    self.context
  }

  pub unsafe fn bind_function_raw(&mut self, name: &CStr, func: Func) {
    let cfunc = JS_NewCFunction2(self.context, Some(func), name.as_ptr(), 1, JSCFunctionEnum_JS_CFUNC_generic, 0);
    self.set_global(name, cfunc);
  }

  pub unsafe fn bind_function_data(&mut self, name: &CStr, func: FuncData, argc: c_int, magic: i16, data: *mut c_void) {
    // TODO: fix this ridiculous memleak
    let mut jsdata = Box::new(JSValue {
      u: JSValueUnion {
        ptr: data,
      },
      tag: JS_TAG_NULL as i64,
    });
    let cfunc = JS_NewCFunctionData(self.context, Some(func), argc, magic as i32, 1, (&mut *jsdata) as *mut JSValue);
    Box::leak(jsdata);
    self.set_global(name, cfunc);
  }

  pub unsafe fn set_global(&mut self, name: &CStr, value: JSValue) {
    let global = JS_GetGlobalObject(self.context);
    let _ret = JS_SetPropertyStr(
      self.context,
      global,
      name.as_ptr(),
      value,
    );
    JS_FreeValue(self.context, global);
  }

  pub unsafe fn get_global(&mut self, name: &CStr) -> ValueRef {
    let global = JS_GetGlobalObject(self.context);
    let ret = JS_GetPropertyStr(
      self.context,
      global,
      name.as_ptr(),
    );
    JS_FreeValue(self.context, global);
    ValueRef::from(self.context, ret)
  }

  pub unsafe fn call(&mut self, value: JSValue) -> JSValue {
    let res = JS_Call(self.context, value, undefined(), 0, std::ptr::null_mut());

    if res.tag as i32 == JS_TAG_EXCEPTION {
      let exc = JS_GetException(self.context);
      let s = jsvalue_to_string(self.context, exc);

      println!("Exception: {}", s);
    }
    res
  }

  pub unsafe fn call_args(&mut self, value: JSValue, mut args: Vec<JSValue>) -> JSValue {
    let res = JS_Call(self.context, value, undefined(), args.len() as i32, args.as_mut_ptr());

    if res.tag as i32 == JS_TAG_EXCEPTION {
      let exc = JS_GetException(self.context);
      let s = jsvalue_to_string(self.context, exc);

      println!("Exception: {}", s);
    }
    res
  }

  pub unsafe fn call_arg2(&mut self, value: JSValue, x: f32, y: f32) -> JSValue {
    let jsx = JS_NewFloat64(self.context, x as f64);
    let jsy = JS_NewFloat64(self.context, y as f64);
    let mut args = [jsx, jsy];
    let ret = JS_Call(self.context, value, undefined(), 2, args.as_mut_ptr());
    JS_FreeValue(self.context, jsx);
    JS_FreeValue(self.context, jsy);
    ret
  } 

  fn eval(&mut self, filename: &str, code: &str) {
    let filename_c = CString::new(filename).unwrap();
    let code_c = CString::new(code).unwrap();

    let value_raw = unsafe {
      JS_Eval(
        self.context,
        code_c.as_ptr(),
        code.len() as _,
        filename_c.as_ptr(),
        JS_EVAL_TYPE_MODULE as i32,
      )
    };
    if value_raw.tag as i32 == JS_TAG_EXCEPTION {
      unsafe {
        let exc = JS_GetException(self.context);
        let s = jsvalue_to_string(self.context, exc);

        println!("exc: {}", s);
      }
    }
    unsafe { JS_FreeValue(self.context, value_raw); };
  }

  pub fn load(&mut self, filename: &str) -> Result<(), std::io::Error> {
    let mut file = File::open(filename)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    self.eval(filename, &contents);
    Ok(())
  }
}

impl Drop for Runtime {
  fn drop(&mut self) {
    unsafe {
      JS_FreeContext(self.context);
      JS_FreeRuntime(self.runtime);
    }
  }
}