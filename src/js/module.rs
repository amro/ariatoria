use std::cell::Ref;
use std::cell::RefCell;
use std::collections::HashMap;
use std::ffi::CStr;
use std::ffi::CString;
use std::fs::File;
use std::io::Read;

use lazy_static::lazy_static;
use libc::c_char;
use libc::c_int;
use libc::c_void;
use libquickjs_sys::*;

pub type GetterMagic = unsafe extern "C" fn(*mut JSContext, JSValue, i32) -> JSValue;
pub type SetterMagic = unsafe extern "C" fn(*mut JSContext, JSValue, JSValue, i32) -> JSValue;
pub type ModuleInit  = unsafe extern "C" fn(*mut JSContext, *mut JSModuleDef) -> c_int;
pub type Finalizer   = unsafe extern "C" fn(*mut JSRuntime, JSValue) -> ();
pub type Constructor = unsafe extern "C" fn(*mut JSContext, JSValue, c_int, *mut JSValue) -> JSValue;
pub type Func        = unsafe extern "C" fn(*mut JSContext, JSValue, c_int, *mut JSValue) -> JSValue;

enum JSCFunctionData {
  MagicGetSet(GetterMagic, SetterMagic, i16),
  Func(u8, Func)
}
pub struct JSCFunctionDef {
  name: &'static CStr,
  data: JSCFunctionData,
}

pub struct FuncDef {
  pub func: Func,
  pub argc: c_int,
  pub name: &'static CStr,
}

impl JSCFunctionDef {
  pub fn magic_getset(name: &'static CStr, fgetter: GetterMagic, fsetter: SetterMagic, magic: i16) -> Self {
    JSCFunctionDef {
      name,
      data: JSCFunctionData::MagicGetSet(fgetter, fsetter, magic)
    }
  }

  pub fn func(name: &'static CStr, func: Func, argc: u8) -> Self {
    JSCFunctionDef {
      name,
      data: JSCFunctionData::Func(argc, func)
    }
  }
  fn get_entry(&self) -> JSCFunctionListEntry {
    match self.data {
        JSCFunctionData::MagicGetSet(fgetter, fsetter, magic) => {
          JSCFunctionListEntry {
          prop_flags: JS_PROP_CONFIGURABLE as u8,
          def_type: JS_DEF_CGETSET_MAGIC as u8,
          name: self.name.as_ptr(),
          magic: magic,
          u: JSCFunctionListEntry__bindgen_ty_1 {
            getset: JSCFunctionListEntry__bindgen_ty_1__bindgen_ty_2 {
              get: JSCFunctionType {
                getter_magic: Some(fgetter),
              },
              set: JSCFunctionType {
                setter_magic: Some(fsetter),
              },
            },
          },
        }
      },
      JSCFunctionData::Func(argc, func) => {
        JSCFunctionListEntry {
          prop_flags: (JS_PROP_CONFIGURABLE | JS_PROP_WRITABLE) as u8,
          def_type: JS_DEF_CFUNC as u8,
          name: self.name.as_ptr(),
          magic: 0,
          u: JSCFunctionListEntry__bindgen_ty_1 {
            func: JSCFunctionListEntry__bindgen_ty_1__bindgen_ty_1 {
              length: argc,
              cproto: JSCFunctionEnum_JS_CFUNC_generic as u8,
              cfunc: JSCFunctionType {
                generic: Some(func)
              }
            }
          }
        }
      }
    }
  }
}

pub struct Module {
  pub name: &'static CStr,
  pub classes: Vec<Class>,
}

pub struct ClassData {
  pub name: &'static CStr,
  pub id: JSClassID,
  pub proto: JSValue,
  pub ctor_args: c_int,
  pub ctor: Constructor,
  pub finalizer: Option<Finalizer>,
  pub entries: Vec<JSCFunctionDef>,
}

pub struct Class {
  data: ClassData,
  list_entries: Vec<JSCFunctionListEntry>,
  pub statics: Vec<FuncDef>,
}

impl Class {
  pub fn new(data: ClassData) -> Self {
    let list_entries: Vec<_> = data.entries.iter().map(JSCFunctionDef::get_entry).collect();
    Class {
      data,
      list_entries,
      statics: Vec::new(),
    }
  }

  pub fn with_statics(self, statics: Vec<FuncDef>) -> Self {
    Class {
      statics,
      ..self
    }
  }
}

pub struct ModuleRegistry {
  modules: RefCell<HashMap<CString, Module>>,
}

impl ModuleRegistry {
  pub fn new() -> Self {
    ModuleRegistry {
      modules: RefCell::new(HashMap::new()),
    }
  }

  pub fn contains(&self, name: &CStr) -> bool {
    self.modules.borrow().contains_key(name)
  }

  pub fn get(&self, name: &CStr) -> Ref<'_, Module> {
    Ref::map(self.modules.borrow(), |x| x.get(name).unwrap())
  }

  pub unsafe fn register(&self, name: &'static CStr, module: Module) {
    self.modules.borrow_mut().insert(name.into(), module);
  }
}

unsafe impl Sync for ModuleRegistry { }

lazy_static! {
  pub static ref REGISTRY: ModuleRegistry = {
    ModuleRegistry::new()
  };
}

unsafe extern "C" fn js_init_helper(ctx: *mut JSContext, m: *mut JSModuleDef) -> c_int {
  let name_atom = JS_GetModuleName(ctx, m);
  let name_cstr  = JS_AtomToCString(ctx, name_atom);
  let module = REGISTRY.get(&CStr::from_ptr(name_cstr));
  for class in &module.classes {
    let class_def = JSClassDef {
      finalizer: class.data.finalizer,
      class_name: class.data.name.as_ptr(),
      call: None,
      gc_mark: None,
      exotic: std::ptr::null_mut(),
    };

    JS_NewClass(JS_GetRuntime(ctx), class.data.id, &class_def);

    log::debug!("Loading: {:?}", CStr::from_ptr(name_cstr));
    JS_SetPropertyFunctionList(ctx, class.data.proto, class.list_entries.as_ptr(), class.list_entries.len() as i32);
    let class_obj = JS_NewCFunction2(ctx, Some(class.data.ctor), class.data.name.as_ptr(), class.data.ctor_args, JSCFunctionEnum_JS_CFUNC_constructor, 0);

    JS_SetConstructor(ctx, class_obj, class.data.proto);
    JS_SetClassProto(ctx, class.data.id, class.data.proto);

    for fdef in &class.statics {
      let cfunc = JS_NewCFunction2(ctx, Some(fdef.func), fdef.name.as_ptr(), fdef.argc, JSCFunctionEnum_JS_CFUNC_generic, 0);
      JS_SetPropertyStr(ctx, class_obj, fdef.name.as_ptr(), cfunc);
    }

    JS_SetModuleExport(ctx, m, class.data.name.as_ptr(), class_obj);
  }
  0
}

pub unsafe extern "C" fn module_loader(ctx: *mut JSContext, cname: *const c_char, _opaque: *mut c_void) -> *mut JSModuleDef {
  let cstr = CStr::from_ptr(cname);
  let name = cstr.to_str().unwrap();
  if REGISTRY.contains(&cstr) {
    let module = REGISTRY.get(&cstr);
    let m = JS_NewCModule(ctx, cname, Some(js_init_helper));
    for class in &module.classes {
      JS_AddModuleExport(ctx, m, class.data.name.as_ptr());
    }
    return m;
  }
  let filename = format!("{}.js", name);
  let mut file = File::open(filename).unwrap();
  let mut contents = String::new();
  file.read_to_string(&mut contents).unwrap();

  let len = contents.len();
  let buf = CString::new(contents).unwrap();

  let flags = (JS_EVAL_TYPE_MODULE | JS_EVAL_FLAG_COMPILE_ONLY) as i32;
  let func_val = JS_Eval(ctx, buf.as_ptr(), len as u64, cname, flags);

  let ret = func_val.u.ptr as *mut JSModuleDef;
  JS_FreeValue(ctx, func_val);
  ret
}