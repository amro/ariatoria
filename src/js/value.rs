use libquickjs_sys::*;

pub struct ValueRef {
  ctx: *mut JSContext,
  js: JSValue,
}

impl ValueRef {
  pub fn from(ctx: *mut JSContext, js: JSValue) -> Self {
    ValueRef {
      ctx,
      js,
    }
  }

  pub fn value(&self) -> JSValue {
    self.js
  }

  #[inline]
  pub fn is_exception(&self) -> bool {
    self.js.tag as i32 == JS_TAG_EXCEPTION
  }
}

impl Drop for ValueRef {
  fn drop(&mut self) {
    unsafe {
      JS_FreeValue(self.ctx, self.js);
    }
  }
}

pub unsafe fn jsvalue_to_string(ctx: *mut JSContext, value: JSValue) -> String {
  let st = JS_ToString(ctx, value);
  let ptr = JS_ToCStringLen2(ctx, std::ptr::null_mut(), st, 0);

  if ptr.is_null() {
    panic!();
  }

  let cstr = std::ffi::CStr::from_ptr(ptr);

  let s = cstr
      .to_str()
      .unwrap()
      .to_string();

  // Free the c string.
  JS_FreeCString(ctx, ptr);
  JS_FreeValue(ctx, st);

  s
}

pub const fn undefined() -> JSValue {
  JSValue {
    u: JSValueUnion { int32: 0 },
    tag: JS_TAG_UNDEFINED as i64,
  }
}