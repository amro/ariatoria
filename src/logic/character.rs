
use crate::{core::{ world::Entity}};
use crate::core::{Resources, Transform, World, input::InputAxis};
use crate::gfx::{spritesheet::{SpritesheetSpec, Spritesheet}, debug::DebugBox, sprite::Sprite, animation::Animation};

use crate::math::{vec2, Collider, Rect};

#[derive(Default)]
pub struct Character { }

impl Character { 
  pub fn new() -> Self {
    Character {

    }
  }
}

pub struct CharacterSystem {

}

impl CharacterSystem {
  pub fn new() -> Self {
    CharacterSystem {

    }
  }

  pub fn spawn_character(&self, resources: &mut Resources, world: &mut World, x: f32, y: f32) -> Entity {
    let tex = resources.texture_system.load("res/sprites/idle_down.png");
    let spritesheet = Spritesheet::from_spec(SpritesheetSpec {
      texture_width: 384,
      texture_height: 48,
      tile_width: 48,
      tile_height: 48
    });
    let coords = (0..7).map(|i| spritesheet.get_coords(i)).collect();
    let entity = world.new_entity();
    world.components.character.insert(entity, Character::new());
    world.components.transform.insert(entity, Transform::of(x, y));
    world.components.sprite.insert(entity, Sprite::new(3, 48.0, 48.0, spritesheet.get_coords(0)));
    world.components.collider.insert(entity, Collider::new(18.0, 10.0, 12.0, 6.0));
    world.components.debug_box.insert(entity, DebugBox(Rect::of(18.0, 10.0, 12.0, 6.0)));
    world.components.texture.insert(entity, tex);
    world.components.animation.insert(entity, Animation::of(coords));

    entity
  }

  pub fn character_spawn(&mut self, resources: &mut Resources, world: &mut World) {
    if world.components.character.iter().count() > 0 {
      return;
    }

    self.spawn_character(resources, world, 128.0, 128.0);
  }

  pub fn character_movement(&mut self, resources: &mut Resources, world: &mut World) {
    let input = &resources.input;
    let mut player = None;

    for (entity, _character) in world.components.character.iter() {
      player = Some(entity);
    }

    if let Some(player) = player {
      let v_amt = match input.get_axis(InputAxis::LeftY) {
        y if y.abs() < 0.1 => 0.0,
        y => y * 2.0
      };
      let h_amt = match input.get_axis(InputAxis::LeftX) {
        x if x.abs() < 0.1 => 0.0,
        x => x * 2.0
      };

      let player_collider = world.components.collider.get(player).unwrap();
      let mut transform = (*world.components.transform.get(player).unwrap()).clone();

      let mut smallest_movement = vec2(h_amt as f32, 0.);

      for (entity, collider) in world.components.collider.iter() {
        if player == entity {
          continue;
        }
        let collider_transform = world.components.transform.get(entity).unwrap();
        if let Some(offset) = player_collider.directional_collision_offset(&transform, smallest_movement, &collider, collider_transform) {
          if offset < smallest_movement {
            smallest_movement = offset;
          }
        }
      }

      transform.position = transform.position + smallest_movement;

      for (entity, collider) in world.components.collider.iter() {
        if player == entity {
          continue;
        }
        let collider_transform = world.components.transform.get(entity).unwrap();
        if let Some((x_offset, _y_offset)) = player_collider.collision_offset(&transform, &collider, collider_transform) {
          transform.position.x += x_offset;
        }
      }

      let mut smallest_movement = vec2(0., v_amt as f32);

      for (entity, collider) in world.components.collider.iter() {
        if player == entity {
          continue;
        }
        let collider_transform = world.components.transform.get(entity).unwrap();
        if let Some(offset) = player_collider.directional_collision_offset(&transform, smallest_movement, &collider, collider_transform) {
          if offset < smallest_movement {
            smallest_movement = offset;
          }
        }
      }

      transform.position = transform.position + smallest_movement;

      for (entity, collider) in world.components.collider.iter() {
        if player == entity {
          continue;
        }
        let collider_transform = world.components.transform.get(entity).unwrap();
        if let Some((_x_offset, y_offset)) = player_collider.collision_offset(&transform, &collider, collider_transform) {
          transform.position.y += y_offset;
        }
      }

      *world.components.transform.get_mut(player).unwrap() = transform;
      
    }
  }
}
