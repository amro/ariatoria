
#![warn(clippy::all)]
#![warn(rust_2018_idioms)]
#![allow(clippy::many_single_char_names)]
#![allow(dead_code)]
#![feature(associated_type_defaults, async_closure, generators, trait_alias)]

mod core;
mod js;
mod logic;
mod game;
mod generational_index;
mod gfx;
mod map;
mod math;

use crate::game::Game;
use crate::gfx::render::Renderer;
use crate::js::Runtime;
use glutin::ContextBuilder;
use glutin::GlProfile;
use glutin::GlRequest;
use glutin::dpi::LogicalPosition;
use glutin::dpi::LogicalSize;
use glutin::event::ElementState;
use glutin::event::Event;
use glutin::event::WindowEvent;
use glutin::event::VirtualKeyCode;
use gilrs::Gilrs;
use glutin::event_loop::ControlFlow;
use glutin::event_loop::EventLoop;
use glutin::window::WindowBuilder;
use pathfinder_geometry::vector::vec2f;

const SCREEN_WIDTH: f64 = 512.0f64;
const SCREEN_HEIGHT: f64 = 512.0f64;

fn invert_y(p: LogicalPosition<f64>, s: LogicalSize<f64>) -> LogicalPosition<f32> {
  LogicalPosition::new(p.x as f32, (s.height - p.y) as f32)
}

fn main() {
  env_logger::init();
  let event_loop = EventLoop::new();
  let window = WindowBuilder::new()
    .with_title("aria")
    .with_decorations(true)
    .with_inner_size(LogicalSize::new(SCREEN_WIDTH, SCREEN_HEIGHT));

  let gl_context = ContextBuilder::new()
    .with_vsync(true)
    .with_gl(GlRequest::Latest)
    .with_gl_profile(GlProfile::Core)
    .build_windowed(window, &event_loop).unwrap();

  let mut context = unsafe { gl_context.make_current().unwrap() };

  Renderer::init(&mut context);



  // let mut cursor_position = LogicalPosition::new(0.0, 0.0);
  let mut cursor_position = vec2f(0.0, 0.0);
  let mut screen_size = LogicalSize::new(0.0, 0.0);

  let gilrs = Gilrs::new().unwrap();
  let runtime = Runtime::new();
  let mut game = Game::new(gilrs, runtime);
  let mut init = false;

  let mut scale_factor = context.window().scale_factor();

  event_loop.run(move |event,_, control_flow| {
    *control_flow = ControlFlow::Wait;
    if !init {
      game.init();
      init = true;
    }
    match event {
      Event::LoopDestroyed => return,
      Event::WindowEvent { event: WindowEvent::Destroyed, .. } => {
        *control_flow = ControlFlow::Exit;
      },
      Event::WindowEvent { event: WindowEvent::ScaleFactorChanged { scale_factor: new_scale_factor, ..}, .. } => {
        scale_factor = new_scale_factor;
        let size = screen_size.to_physical(scale_factor);
        game.resize(size.width as u32, size.height as u32);
        Renderer::resize(size.width as i32, size.height as i32);
        context.resize(size);
      },
      Event::WindowEvent { event: WindowEvent::Resized(size), .. } => {
        screen_size = size.to_logical(scale_factor);
        game.resize(size.width as u32, size.height as u32);
        Renderer::resize(size.width as i32, size.height as i32);
        context.resize(size);
      },
      Event::WindowEvent { event: WindowEvent::CursorMoved { position, .. }, .. } => {
        // let tmp = invert_y(position.to_logical(scale_factor), screen_size);
        let new = vec2f(position.x as f32, position.y as f32);
        game.mouse_move(cursor_position, new);
        cursor_position = new;
      },
      Event::WindowEvent { event: WindowEvent::MouseInput { state, button, .. }, .. } => {
        match state {
          ElementState::Pressed => game.mouse_down(button, cursor_position),
          ElementState::Released => game.mouse_up(button, cursor_position),
        }
      }
      Event::WindowEvent { event: WindowEvent::ReceivedCharacter(_c), .. } => {
      },
      Event::WindowEvent { event: WindowEvent::KeyboardInput {input, .. }, ..} => {
        if let Some(keycode) = input.virtual_keycode {
          game.handle_key(keycode, input.state);
          if input.state == ElementState::Pressed {
            if keycode == VirtualKeyCode::Escape {
              *control_flow = ControlFlow::Exit;
            }
          }
        }
      },
      Event::MainEventsCleared => {
        unsafe {
          gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
        };
        game.tick();
        context.swap_buffers().unwrap()
      }
      _ => {
      }
    }
  });
}

