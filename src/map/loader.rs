use crate::core::{transform::Transform, World, Resources};
use crate::{gfx::{spritesheet::{SpritesheetSpec, Spritesheet}, debug::DebugBox, sprite::Sprite}, math::{Rect, Collider}, map::{Tsx, Tmx}};
use std::collections::HashMap;

pub struct MapLoader {
  requested_map: Option<String>,
  loaded: bool,
}

impl MapLoader {
  pub fn new() -> Self {
    MapLoader {
      requested_map: Some("res/maps/rogue.tmx".to_string()),
      loaded: false,
    }
  }

  pub fn load(&mut self, resources: &mut Resources, world: &mut World) {
    if self.loaded {
      return;
    }

    let requested_map = self.requested_map.as_ref().unwrap();
    let tmx_handle = resources.tmx.get_or_load(requested_map, |spec| Tmx::from_file(spec).unwrap());
    let tmx = resources.tmx.get(tmx_handle).unwrap();

    let mut tiles = Vec::new();
    let mut colliders = Vec::new();
    let (tile_width, tile_height) = tmx.tile_size();

    let mut lookup = HashMap::new();

    for tileset in tmx.tilesets.iter() {
      let tsx_handle = resources.tsx.get_or_load(&format!("res/maps/{}", tileset.source), |spec| Tsx::from_file(spec).unwrap());
      let tsx = resources.tsx.get(tsx_handle).unwrap();
      let spritesheet = Spritesheet::from_spec(SpritesheetSpec {
        texture_width: tsx.image.width,
        texture_height: tsx.image.height,
        tile_width: tsx.tilewidth,
        tile_height: tsx.tileheight
      });
      for i in tileset.first_gid .. (tileset.first_gid + tsx.tilecount) {
        lookup.insert(i, (tsx_handle, tileset.first_gid, spritesheet.clone()));
      }
    }

    for (z, layer) in tmx.layers.iter().enumerate() {
      for chunk in layer.data.chunks.iter() {
        let splot: Vec<_> = chunk.content.split('\n').collect();
        for (y, row) in splot.iter().rev().enumerate() {
          for (x, col) in row.split(',').enumerate() {
            if col != "0" && !col.is_empty()  {
              let idx = col.parse::<usize>().unwrap();
              let (tileset_handle, tileset_offset, spritesheet) = lookup.get(&idx).unwrap();
              let tileset = resources.tsx.get(*tileset_handle).unwrap();
              let texture = resources.texture_system.load(&format!("res/tilesets/{}", tileset.image.source));
              let id = idx - tileset_offset;
              let transform = Transform::of((chunk.x + x as isize) as f32 * tile_width as f32, (chunk.y + y as isize) as f32 * tile_height as f32);
              for td in &tileset.tile_details {
                if td.id == id {
                  for obj_g in &td.objects {
                    for obj in &obj_g.objects {
                      let x = obj.x as f32;
                      let y = (16 - (obj.height + obj.y)) as f32;
                      let width = obj.width as f32;
                      let height = obj.height as f32;
                      let collider = Collider::new(x, y, width, height);
                      colliders.push((
                        collider,
                        transform.clone(),
                        DebugBox(Rect::of(x, y, width, height)),
                      ));
                    }
                  }
                }
              }
              let coords = spritesheet.get_coords(id as u32);
              tiles.push((
                Sprite::new(z as u8, tileset.tilewidth as f32, tileset.tileheight as f32, coords),
                texture,
                transform,
              ));
            }
          }
        }
      }
    }
    for (sprite, texture, transform) in tiles {
      let entity = world.new_entity();
      world.components.sprite.insert(entity, sprite);
      world.components.texture.insert(entity, texture);
      world.components.transform.insert(entity, transform);
    }
    for (collider, transform, dbox) in colliders {
      let entity = world.new_entity();
      world.components.collider.insert(entity, collider);
      world.components.transform.insert(entity, transform);
      world.components.debug_box.insert(entity, dbox);
    }
    self.loaded = true;
  }
}


