
use serde::Deserialize;
use std::fs::File;

use crate::core::asset;

use log::error;

#[derive(Clone, Debug, Default, Deserialize)]
#[serde(rename = "properties")]
pub struct Properties {
  #[serde(default, rename = "property")]
  pub properties: Vec<Property>
}

#[derive(Clone, Debug, Default, Deserialize)]
#[serde(rename = "property")]
pub struct Property {
  pub name: String,
  pub value: String,
}

#[derive(Clone, Debug, Default, Deserialize)]
#[serde(rename = "object")]
pub struct Object {
  #[serde(default, rename = "type")]
  pub kind: String,
  pub x: isize,
  pub y: isize,
  pub width: isize,
  pub height: isize,

  #[serde(default)]
  pub properties: Properties,
}

#[derive(Clone, Debug, Default, Deserialize)]
#[serde(rename = "objectgroup")]
pub struct ObjectGroup {
  #[serde(default)]
  pub name: String,
  #[serde(rename = "object")]
  pub objects: Vec<Object>,
}

#[derive(Debug, Default, Clone, Deserialize)]
#[serde(rename = "objectgroup", default)]
pub struct TileDetails {
  pub id: usize,

  #[serde(rename = "objectgroup")]
  pub objects: Vec<ObjectGroup>,
}

#[derive(Debug, Default, Clone, Deserialize)]
#[serde(rename = "image", default)]
pub struct Image {
  pub source: String,

  pub width: u32,
  pub height: u32,
}

#[derive(Debug, Default, Clone, Deserialize)]
#[serde(rename = "tileset", default)]
pub struct Tsx {
  pub name: String,
  pub tilewidth: u32,
  pub tileheight: u32,
  pub tilecount: usize,
  #[serde(rename = "tile")]
  pub tile_details: Vec<TileDetails>,

  pub image: Image,
}

#[derive(Debug, Default, Deserialize)]
#[serde(rename = "chunk", default)]
pub struct Chunk {
  pub x: isize,
  pub y: isize,
  pub width: usize,
  pub height: usize,
  #[serde(rename = "$value")]
  pub content: String,
}

#[derive(Debug, Default, Deserialize)]
#[serde(rename = "data")]
pub struct Data {
  #[serde(rename = "chunk")]
  pub chunks: Vec<Chunk>,
}

#[derive(Debug, Default, Deserialize)]
#[serde(rename = "layer")]
pub struct Layer {
  pub name: String,
  pub width: u32,
  pub height: u32,

  #[serde(rename = "data", default)]
  pub data: Data,
}

#[derive(Debug, Default, Clone, Deserialize)]
#[serde(rename = "tileset", default)]
pub struct TilesetReference {
  pub source: String,

  #[serde(rename = "firstgid")]
  pub first_gid: usize,
}

#[derive(Debug, Default, Deserialize)]
#[serde(rename = "map", default)]
pub struct Tmx {
  pub width: usize,
  pub height: usize,

  #[serde(rename="tilewidth")]
  pub tile_width: usize,
  #[serde(rename="tileheight")]
  pub tile_height: usize,

  #[serde(rename = "tileset", default)]
  pub tilesets: Vec<TilesetReference>,

  #[serde(rename = "layer", default)]
  pub layers: Vec<Layer>,

  #[serde(rename = "objectgroup", default)]
  pub object_groups: Vec<ObjectGroup>,
}

impl Tsx {
  pub fn from_file(file: &str) -> Result<Self, asset::Error> {
    serde_xml_rs::from_reader(File::open(file)
      .map_err(|_| { 
        error!("File not found: {}", file);
        asset::Error::NotFound
      })?)
    .map_err(|e| { 
      error!("Invalid format: {}", e);
      asset::Error::InvalidFormat
    })
  }
}

impl Tmx {
  pub fn from_file(file: &str) -> Result<Self, asset::Error> {
    serde_xml_rs::from_reader(File::open(file)
      .map_err(|_| { 
        error!("File not found: {}", file);
        asset::Error::NotFound
      })?)
    .map_err(|e| { 
      error!("Invalid format: {}", e);
      asset::Error::InvalidFormat
    })
  }

  pub fn tile_size(&self) -> (usize, usize) {
    (self.tile_width, self.tile_height)
  }

}