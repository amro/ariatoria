
use crate::core::Transform;
use crate::math::vec2;
use crate::math::Vector2;
use crate::math::is_zero;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Collider {
  pub x: f32,
  pub y: f32,
  pub width: f32,
  pub height: f32,
  pub sides: Vec<(Vector2, Vector2)>,
  pub points: Vec<Vector2>,
}

struct LineSegment {
  start: Vector2,
  direction: Vector2,
}

// pub fn distance(p1: &Vector2, p2: &Vector2) -> f32 {
//   ((p2.x - p1.x).powf(2.) + (p2.y - p1.y).powf(2.)).sqrt()
// }

impl LineSegment {
  pub fn new(start: Vector2, end: Vector2) -> Self {
    LineSegment {
      start,
      direction: Vector2::new(end.x - start.x, end.y - start.y)
    }
  }
}

pub enum IntersectionResult {
  Overlapping(f32, f32),
  Intersecting(f32, f32),
  NoIntersection,
}

fn intersection(ls0: LineSegment, ls1: LineSegment) -> IntersectionResult {
  let p = ls0.start;
  let r = ls0.direction;
  let q = ls1.start;
  let s = ls1.direction;

  // r × s
  let r_s = r.cross(&s);
  // (q − p) × r
  let q_p_r = (q - p).cross(&r); //cross(subtractPoints(q, p), r);

  if is_zero(r_s) && is_zero(q_p_r) {
    // t0 = (q − p) · r / (r · r)
    // const t0 = dot(subtractPoints(q, p), r) / dot(r, r);
    // let t0 = (q - p).dot(&r) / r.dot(&r);

    // t1 = (q + s − p) · r / (r · r) = t0 + s · r / (r · r)
    // const t1 = t0 + dot(s, r) / dot(r, r);
    // let t1 = t0 + s.dot(&r) / r.dot(&r);

    // NOTE(tp): For some reason (which I haven't spotted yet), the above t0 and hence t1 is wrong
    // So resorting to calculating it 'backwards'
    let t1 = (q + (s - p)).dot(&r) / r.dot(&r);
    // let t1 = dot(addPoints(q, subtractPoints(s, p)), r) / dot(r, r);
    let t0 = t1 - s.dot(&r) / r.dot(&r);
    // let t0 = t1 - dot(s, r) / dot(r, r);

    if t0 >= 0. && t0 <= 1. || t1 >= 0. && t1 <= 1. {
      return IntersectionResult::Overlapping(t0, t1);
    }

    return IntersectionResult::NoIntersection;
  }

  if is_zero(r_s) && !is_zero(q_p_r) {
    return IntersectionResult::NoIntersection;
  }
  // t = (q − p) × s / (r × s)
  let t = (q - p).cross(&s) / r.cross(&s);
  // let t = cross(subtractPoints(q, p), s) / cross(r, s);

  // u = (q − p) × r / (r × s)
  let u = (q - p).cross(&r) / r.cross(&s);
  // let u = cross(subtractPoints(q, p), r) / cross(r, s);

  if !is_zero(r_s) && t >= 0. && t <= 1. && u >= 0. && u <= 1. {
    return IntersectionResult::Intersecting(t, u);
  }

  IntersectionResult::NoIntersection
}

pub fn intersect(p1_start: Vector2, p1_end: Vector2, p2_start: Vector2, p2_end: Vector2) -> IntersectionResult {
  intersection(LineSegment::new(p1_start, p1_end), LineSegment::new(p2_start, p2_end))
}

impl Collider {
  pub fn new(x: f32, y: f32, width: f32, height: f32) -> Self {
    let mut sides = Vec::new();
    sides.push((vec2(x, y), vec2(x, y + height)));
    sides.push((vec2(x, y), vec2(x + width, y)));
    sides.push((vec2(x + width, y), vec2(x + width, y + height)));
    sides.push((vec2(x + width, y + height), vec2(x, y + height)));

    let mut points = Vec::new();
    points.push(vec2(x, y));
    points.push(vec2(x + width, y));
    points.push(vec2(x + width, y + height));
    points.push(vec2(x, y +  height));
    Collider {
      x,
      y,
      width,
      height,
      sides,
      points
    }
  }
  pub fn collision_offset(&self, transform: &Transform, other: &Collider, other_transform: &Transform) -> Option<(f32, f32)>{
    let tr = transform.position;
    let x = tr.x + self.x;
    let y = tr.y + self.y;

    let ctr = other_transform.position;
    let cx = ctr.x + other.x;
    let cy = ctr.y + other.y;
    if x < cx + other.width && x + self.width > cx && y < cy + other.height && y + self.height > cy {
      let right_offset = (cx + other.width) - x;
      let left_offset = cx - (x + self.width);

      let bottom_offset = (cy + other.height) - y;
      let top_offset = cy - (y + self.height);

      let x_offset = if right_offset.abs() > left_offset.abs() {
        left_offset
      } else {
        right_offset
      };

      let y_offset = if top_offset.abs() > bottom_offset.abs() {
        bottom_offset
      } else {
        top_offset
      };
      Some((x_offset, y_offset))
    } else {
      None
    }
  }

  pub fn directional_collision_offset(&self, transform: &Transform, movement: Vector2, other: &Collider, other_transform: &Transform) -> Option<Vector2> {
    let mut smallest_movement: Option<Vector2> = None;
    let mut all_collide = true;
    for point in &self.points {
      let adjusted_point = transform.position + point;
      for (side_start, side_end) in &other.sides {
        let adjusted_side_start = other_transform.position + side_start;
        let adjusted_side_end = other_transform.position + side_end;

        match intersect(adjusted_point, adjusted_point + movement, adjusted_side_start, adjusted_side_end) {
          IntersectionResult::Intersecting(t, _) | IntersectionResult::Overlapping(t, _) => {
            let adjusted_movement = movement * t;
            if let Some(ref mut smallest) = smallest_movement {
              if adjusted_movement.len() < smallest.len() {
                *smallest = adjusted_movement;
              }
            } else {
              smallest_movement = Some(adjusted_movement);
            }
          },
          IntersectionResult::NoIntersection => {
            all_collide = false;
          },
        }
      }
    }
    // smallest_movement.map(|v| v * 0.8)
    if all_collide {
      smallest_movement.map(|v| v * 0.8)
    } else {
      None
    }
  }
}


#[cfg(test)]
mod tests {
  use super::*;
  use crate::math::vec2;
  #[test]
  fn test_intersect() {

    // let one = ls0.start + (t * ls0.direction.normalized());
    let res = intersect(vec2(0., 0.), vec2(1., 1.), vec2(0.5, 0.), vec2(0.5, 1.));

    if let IntersectionResult::Intersecting(_, _) = res {

    } else {
      panic!("boo");
    }
  }
}