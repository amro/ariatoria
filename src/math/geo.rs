use std::ops::Add;

use pathfinder_geometry::rect::RectF;
use pathfinder_geometry::vector::vec2f;

#[derive(Debug, Clone, Copy)]
pub struct Size {
  pub width: f32,
  pub height: f32,
}

impl Add<Size> for Size {
    type Output = Size;

    fn add(self, rhs: Size) -> Self::Output {
      Size::of(self.width + rhs.width, self.height + rhs.height)
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Point {
  pub x: f32,
  pub y: f32,
}

impl Point {
  pub const ORIGIN: Point = Point::of(0.0, 0.0);
  pub const fn of(x: f32, y: f32) -> Self {
    Point {
      x, y
    }
  }
}

#[derive(Clone, Copy, Debug)]
pub struct Rect<T> {
  pub x: T,
  pub y: T,
  pub width: T,
  pub height: T,
}

impl <T> Rect<T> {
  pub fn of(x: T, y: T, width: T, height: T) -> Self {
    Rect {
      x,
      y,
      width,
      height,
    }
  }
}

impl Rect<f32> {
  pub fn new() -> Self {
    Rect {
      x: 0.0,
      y: 0.0,
      width: 0.0,
      height: 0.0,
    }
  }

  pub fn to_rectf(&self) -> RectF {
    RectF::new(vec2f(self.x, self.y), vec2f(self.width, self.height))
  }

  pub fn from_size(size: Size) -> Self {
    Rect {
      x: 0.0,
      y: 0.0,
      width: size.width,
      height: size.height,
    }
  }
}

impl Size {
  pub const MIN: Self = Self::of(0.0, 0.0);
  
  pub const fn of(width: f32, height: f32) -> Self {
    Size {
      width,
      height,
    }
  }
  pub fn at(&self, point: Point) -> Rect<f32> {
    Rect::of(point.x, point.y, self.width, self.height)
  }
}