
mod geo;
mod vec;
mod collision;

pub use vec::*;
pub use geo::Rect;
pub use geo::Size;
pub use geo::Point;
pub use collision::Collider;

pub fn is_zero(f: f32) -> bool {
  f.abs() < std::f32::EPSILON
}

pub mod bindings {
  use std::ffi::CString;

  use byte_strings::c_str;
  use libc::c_int;
  use libc::c_void;
  use libquickjs_sys::*;
  use pathfinder_geometry::rect::RectF;
  use pathfinder_geometry::vector::Vector2F;
  use crate::js::new_obj_helper;
  use crate::js::new_obj_proto;
  use crate::js::Class;
  use crate::js::ClassData;
  use crate::js::JSCFunctionDef;
  use crate::js::FuncDef;
  use crate::js::undefined;
  use crate::math::Matrix;
  use crate::math::Vector;

  pub static mut VEC2F_ID: JSClassID = 0;
  pub static mut VEC2F_PROTO: JSValue = undefined();

  pub static mut VEC_ID: JSClassID = 0;
  pub static mut VEC_PROTO: JSValue = undefined();

  pub static mut RECT_ID: JSClassID = 0;
  pub static mut RECT_PROTO: JSValue = undefined();

  pub static mut MATRIX_ID: JSClassID = 0;
  pub static mut MATRIX_PROTO: JSValue = undefined();

  pub fn get_classes(ctx: *mut JSContext) -> Vec<Class> {
    unsafe { 
      JS_NewClassID(&mut VEC2F_ID);
      JS_NewClassID(&mut RECT_ID);

      VEC2F_PROTO = JS_NewObject(ctx);
      RECT_PROTO = JS_NewObject(ctx);

      vec![
        Class::new(ClassData {
          id: VEC2F_ID,
          proto: VEC2F_PROTO,
          name: c_str!("Vector2F"),
          ctor: js_vec2f_ctor,
          ctor_args: 2,
          finalizer: Some(js_vec2f_finalizer),
          entries: vec![
            JSCFunctionDef::magic_getset(c_str!("x"), js_vec2f_get_xy, js_vec2f_set_xy, 0),
            JSCFunctionDef::magic_getset(c_str!("y"), js_vec2f_get_xy, js_vec2f_set_xy, 1),
            JSCFunctionDef::func(c_str!("toString"), js_vec2f_tostring, 0),
            JSCFunctionDef::func(c_str!("add"), js_vec2f_add, 0),
            JSCFunctionDef::func(c_str!("sub"), js_vec2f_sub, 0),
          ]
        }),
        Class::new(ClassData {
          id: VEC_ID,
          proto: VEC_PROTO,
          name: c_str!("Vector"),
          ctor: js_vec_ctor,
          ctor_args: 2,
          finalizer: Some(js_vec_finalizer),
          entries: vec![
            JSCFunctionDef::magic_getset(c_str!("x"), js_vec_get_xyz, js_vec_set_xyz, 0),
            JSCFunctionDef::magic_getset(c_str!("y"), js_vec_get_xyz, js_vec_set_xyz, 1),
            JSCFunctionDef::magic_getset(c_str!("z"), js_vec_get_xyz, js_vec_set_xyz, 2),
            JSCFunctionDef::func(c_str!("toString"), js_vec_tostring, 0),
            JSCFunctionDef::func(c_str!("add"), js_vec_add, 0),
            JSCFunctionDef::func(c_str!("sub"), js_vec_sub, 0),
          ]
        }),
        Class::new(ClassData {
          id: MATRIX_ID,
          proto: MATRIX_PROTO,
          name: c_str!("Matrix"),
          ctor: js_matrix_ctor,
          ctor_args: 2,
          finalizer: Some(js_matrix_finalizer),
          entries: vec![
            JSCFunctionDef::func(c_str!("inverse"), js_matrix_inverse, 0),
            JSCFunctionDef::func(c_str!("mul"), js_matrix_mul, 0),
          ]
        }).with_statics(vec![
          FuncDef {
            name: c_str!("translation"),
            argc: 1,
            func: js_matrix_translation,
          },
          FuncDef {
            name: c_str!("scale"),
            argc: 1,
            func: js_matrix_scale,
          },
          FuncDef {
            name: c_str!("simpleOrtho"),
            argc: 1,
            func: js_matrix_simple_ortho,
          },
        ]),
        Class::new(ClassData {
          id: RECT_ID,
          proto: RECT_PROTO,
          name: c_str!("RectF"),
          ctor: js_rect_ctor,
          ctor_args: 2,
          finalizer: Some(js_rect_finalizer),
          entries: vec![
            JSCFunctionDef::func(c_str!("toString"), js_rect_tostring, 0),
          ]
        }),
      ]
    }
  }

  //----- vec2f

  unsafe extern "C" fn js_vec2f_ctor(ctx: *mut JSContext, new_target: JSValue, argc: c_int, argv: *mut JSValue) -> JSValue {
    let (mut x, mut y) = (0.0, 0.0);
    
    if argc < 0 {
      return undefined();
    }

    JS_ToFloat64(ctx, &mut x, *argv);
    JS_ToFloat64(ctx, &mut y, *(argv.add(1)));

    let p = Box::new(Vector2F::new(
      x as f32,
      y as f32,
    ));

    let raw = Box::into_raw(p) as *mut c_void;
    new_obj_helper(ctx, new_target, VEC2F_ID, raw)
  }

  unsafe extern "C" fn js_vec2f_finalizer(_rt: *mut JSRuntime, value: JSValue) {
    Box::from_raw(JS_GetOpaque(value, VEC2F_ID) as *mut Vector2F);
  }

  unsafe extern "C" fn js_vec2f_get_xy(ctx: *mut JSContext, this_val: JSValue, magic: c_int) -> JSValue {
    let p: &mut Vector2F = &mut *(JS_GetOpaque(this_val, VEC2F_ID) as *mut Vector2F);
    if magic == 0 {
      JS_NewFloat64(ctx, p.x() as f64)
    } else {
      JS_NewFloat64(ctx, p.y() as f64)
    }
  }

  unsafe extern "C" fn js_vec2f_set_xy(ctx: *mut JSContext, this_val: JSValue, val: JSValue, magic: c_int) -> JSValue {
    let p: &mut Vector2F = &mut *(JS_GetOpaque(this_val, VEC2F_ID) as *mut Vector2F);
    let mut i: f64 = 0.0;
    JS_ToFloat64(ctx, &mut i, val);
    if magic == 0 {
      p.set_x(i as f32);
    } else {
      p.set_y(i as f32);
    }
    undefined()
  }

  unsafe extern "C" fn js_vec2f_tostring(ctx: *mut JSContext, this: JSValue, _argc: c_int, _argv: *mut JSValue) -> JSValue {
    let vec = &mut *(JS_GetOpaque(this, VEC2F_ID) as *mut Vector2F);
    let s = format!("{:?}", vec);
    let cs = CString::new(s).unwrap();
    JS_NewString(ctx, cs.as_ptr())
  }

  unsafe extern "C" fn js_vec2f_add(ctx: *mut JSContext, this: JSValue, _argc: c_int, argv: *mut JSValue) -> JSValue {
    let this = &mut *(JS_GetOpaque(this, VEC2F_ID) as *mut Vector2F);
    let vec = *(JS_GetOpaque(*argv, VEC2F_ID) as *mut Vector2F).clone();
    let newvec = Box::new(*this + vec);
    new_obj_proto(ctx, VEC2F_PROTO, VEC2F_ID, newvec)
  }

  unsafe extern "C" fn js_vec2f_sub(ctx: *mut JSContext, this: JSValue, _argc: c_int, argv: *mut JSValue) -> JSValue {
    let this = &mut *(JS_GetOpaque(this, VEC2F_ID) as *mut Vector2F);
    let vec = *(JS_GetOpaque(*argv, VEC2F_ID) as *mut Vector2F).clone();
    let newvec = Box::new(*this - vec);
    new_obj_proto(ctx, VEC2F_PROTO, VEC2F_ID, newvec)
  }

  //----- vector

  unsafe extern "C" fn js_vec_ctor(ctx: *mut JSContext, new_target: JSValue, argc: c_int, argv: *mut JSValue) -> JSValue {
    let (mut x, mut y, mut z) = (0.0, 0.0, 0.0);
    
    if argc != 3 {
      return undefined();
    }

    JS_ToFloat64(ctx, &mut x, *argv);
    JS_ToFloat64(ctx, &mut y, *(argv.add(1)));
    JS_ToFloat64(ctx, &mut z, *(argv.add(2)));

    let p = Box::new(Vector::new(
      x as f32,
      y as f32,
      z as f32,
    ));

    let raw = Box::into_raw(p) as *mut c_void;
    new_obj_helper(ctx, new_target, VEC_ID, raw)
  }

  unsafe extern "C" fn js_vec_finalizer(_rt: *mut JSRuntime, value: JSValue) {
    Box::from_raw(JS_GetOpaque(value, VEC_ID) as *mut Vector);
  }

  unsafe extern "C" fn js_vec_get_xyz(ctx: *mut JSContext, this_val: JSValue, magic: c_int) -> JSValue {
    let p: &mut Vector = &mut *(JS_GetOpaque(this_val, VEC_ID) as *mut Vector);
    match magic {
      0 => JS_NewFloat64(ctx, p.x as f64),
      1 => JS_NewFloat64(ctx, p.y as f64),
      _ => JS_NewFloat64(ctx, p.z as f64),
    }
  }

  unsafe extern "C" fn js_vec_set_xyz(ctx: *mut JSContext, this_val: JSValue, val: JSValue, magic: c_int) -> JSValue {
    let p: &mut Vector = &mut *(JS_GetOpaque(this_val, VEC_ID) as *mut Vector);
    let mut i: f64 = 0.0;
    JS_ToFloat64(ctx, &mut i, val);
    match magic {
      0 => p.x = i as f32,
      1 => p.y = i as f32,
      _ => p.z = i as f32,
    }
    undefined()
  }

  unsafe extern "C" fn js_vec_tostring(ctx: *mut JSContext, this: JSValue, _argc: c_int, _argv: *mut JSValue) -> JSValue {
    let vec = &mut *(JS_GetOpaque(this, VEC_ID) as *mut Vector);
    let s = format!("{:?}", vec);
    let cs = CString::new(s).unwrap();
    JS_NewString(ctx, cs.as_ptr())
  }

  unsafe extern "C" fn js_vec_add(ctx: *mut JSContext, this: JSValue, _argc: c_int, argv: *mut JSValue) -> JSValue {
    let this = &mut *(JS_GetOpaque(this, VEC_ID) as *mut Vector);
    let vec = *(JS_GetOpaque(*argv, VEC_ID) as *mut Vector).clone();
    let newvec = Box::new(*this + vec);
    new_obj_proto(ctx, VEC_PROTO, VEC_ID, newvec)
  }

  unsafe extern "C" fn js_vec_sub(ctx: *mut JSContext, this: JSValue, _argc: c_int, argv: *mut JSValue) -> JSValue {
    let this = &mut *(JS_GetOpaque(this, VEC_ID) as *mut Vector);
    let vec = *(JS_GetOpaque(*argv, VEC_ID) as *mut Vector).clone();
    let newvec = Box::new(*this - vec);
    new_obj_proto(ctx, VEC_PROTO, VEC_ID, newvec)
  }

  //----------- rect

  unsafe extern "C" fn js_rect_tostring(ctx: *mut JSContext, this: JSValue, _argc: c_int, _argv: *mut JSValue) -> JSValue {
    let vec = &mut *(JS_GetOpaque(this, RECT_ID) as *mut RectF);
    let s = format!("{:?}", vec);
    let cs = CString::new(s).unwrap();
    JS_NewString(ctx, cs.as_ptr())
  }

  unsafe extern "C" fn js_rect_finalizer(_rt: *mut JSRuntime, value: JSValue) {
    Box::from_raw(JS_GetOpaque(value, RECT_ID) as *mut RectF);
  }

  unsafe extern "C" fn js_rect_ctor(ctx: *mut JSContext, new_target: JSValue, argc: c_int, argv: *mut JSValue) -> JSValue {
    if argc != 2 {
      return undefined();
    }

    let vec1 = *(JS_GetOpaque(*argv, VEC2F_ID) as *mut Vector2F).clone();
    let vec2 = *(JS_GetOpaque(*argv.add(1), VEC2F_ID) as *mut Vector2F).clone();
    let c = Box::new(RectF::new(vec1, vec2));

    let raw = Box::into_raw(c) as *mut c_void;
    new_obj_helper(ctx, new_target, RECT_ID, raw)
  }

  //-----------

  //----------- matrix

  unsafe extern "C" fn js_matrix_finalizer(_rt: *mut JSRuntime, value: JSValue) {
    Box::from_raw(JS_GetOpaque(value, MATRIX_ID) as *mut Matrix);
  }

  unsafe extern "C" fn js_matrix_ctor(ctx: *mut JSContext, new_target: JSValue, argc: c_int, _argv: *mut JSValue) -> JSValue {
    if argc > 0 {
      return undefined();
    }

    let m = Box::new(Matrix::identity());

    let raw = Box::into_raw(m) as *mut c_void;
    new_obj_helper(ctx, new_target, MATRIX_ID, raw)
  }

  unsafe extern "C" fn js_matrix_translation(ctx: *mut JSContext, _this: JSValue, argc: c_int, argv: *mut JSValue) -> JSValue {
    if argc != 1 {
      return undefined();
    }

    let vec = &*(JS_GetOpaque(*argv, VEC2F_ID) as *mut Vector);

    let p = Box::new(Matrix::translation(&vec));
    new_obj_proto(ctx, MATRIX_PROTO, MATRIX_ID, p)
  }

  unsafe extern "C" fn js_matrix_scale(ctx: *mut JSContext, _this: JSValue, argc: c_int, argv: *mut JSValue) -> JSValue {
    if argc != 1 {
      return undefined();
    }

    let vec = &*(JS_GetOpaque(*argv, VEC2F_ID) as *mut Vector);

    let p = Box::new(Matrix::scale(&vec));
    new_obj_proto(ctx, MATRIX_PROTO, MATRIX_ID, p)
  }

  unsafe extern "C" fn js_matrix_simple_ortho(ctx: *mut JSContext, _this: JSValue, argc: c_int, argv: *mut JSValue) -> JSValue {
    if argc != 2 {
      return undefined();
    }

    let (mut width, mut height) = (0.0, 0.0);
    
    JS_ToFloat64(ctx, &mut width, *argv);
    JS_ToFloat64(ctx, &mut height, *(argv.add(1)));

    let p = Box::new(Matrix::simple_ortho(width as f32, height as f32));
    new_obj_proto(ctx, MATRIX_PROTO, MATRIX_ID, p)
  }

  unsafe extern "C" fn js_matrix_mul(ctx: *mut JSContext, this: JSValue, _argc: c_int, argv: *mut JSValue) -> JSValue {
    let this = &mut *(JS_GetOpaque(this, MATRIX_ID) as *mut Matrix);
    let mat = *(JS_GetOpaque(*argv, MATRIX_ID) as *mut Matrix).clone();
    let newmat = Box::new(*this * mat);
    new_obj_proto(ctx, MATRIX_PROTO, MATRIX_ID, newmat)
  }

  unsafe extern "C" fn js_matrix_inverse(ctx: *mut JSContext, this: JSValue, _argc: c_int, _argv: *mut JSValue) -> JSValue {
    let this = &mut *(JS_GetOpaque(this, MATRIX_ID) as *mut Matrix);
    let newmat = Box::new(this.inverse());
    new_obj_proto(ctx, MATRIX_PROTO, MATRIX_ID, newmat)
  }

  //-----------
}