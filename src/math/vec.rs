use auto_ops::impl_op_ex;

use std::ops::Add;
use std::ops::Mul;
use std::{cmp::Ordering, ops::Sub};

#[repr(C)]
#[derive(Clone, Copy, Default, Debug, PartialEq)]
pub struct Vector2 {
  pub x: f32,
  pub y: f32,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Vector {
  pub x: f32,
  pub y: f32,
  pub z: f32
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Quaternion {
  pub x: f32,
  pub y: f32,
  pub z: f32,
  pub w: f32
}

#[derive(Clone, Copy, Debug)]
pub struct Matrix {
  pub values: [f32; 16]
}

pub const IDENTITY_MATRIX: Matrix = Matrix {
  values: [
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0,
    0.0, 0.0, 0.0, 1.0
  ],
};

impl Matrix {
  pub fn identity() -> Self {
    IDENTITY_MATRIX
  }
  
  pub fn translation(v: &Vector) -> Self {
    Matrix {
      values: [
        1.0, 0.0, 0.0, v.x,
        0.0, 1.0, 0.0, v.y,
        0.0, 0.0, 1.0, v.z,
        0.0, 0.0, 0.0, 1.0
      ]
    }
  }

  pub fn scale(v: &Vector) -> Self {
    Matrix {
      values: [
        v.x, 0.0, 0.0, 0.0,
        0.0, v.y, 0.0, 0.0,
        0.0, 0.0, v.z, 0.0,
        0.0, 0.0, 0.0, 1.0
      ]
    }
  }
  
  pub fn ptr(&self) -> *const f32 {
    self.values.as_ptr()
  }

  pub fn inverse(&self) -> Self {
    let mut inv = [0.0; 16];
    let mut out = [0.0; 16];
    let m = self.values;

    inv[0] = m[5]  * m[10] * m[15] - 
             m[5]  * m[11] * m[14] - 
             m[9]  * m[6]  * m[15] + 
             m[9]  * m[7]  * m[14] +
             m[13] * m[6]  * m[11] - 
             m[13] * m[7]  * m[10];

    inv[4] = -m[4]  * m[10] * m[15] + 
              m[4]  * m[11] * m[14] + 
              m[8]  * m[6]  * m[15] - 
              m[8]  * m[7]  * m[14] - 
              m[12] * m[6]  * m[11] + 
              m[12] * m[7]  * m[10];

    inv[8] = m[4]  * m[9] * m[15] - 
             m[4]  * m[11] * m[13] - 
             m[8]  * m[5] * m[15] + 
             m[8]  * m[7] * m[13] + 
             m[12] * m[5] * m[11] - 
             m[12] * m[7] * m[9];

    inv[12] = -m[4]  * m[9] * m[14] + 
               m[4]  * m[10] * m[13] +
               m[8]  * m[5] * m[14] - 
               m[8]  * m[6] * m[13] - 
               m[12] * m[5] * m[10] + 
               m[12] * m[6] * m[9];

    inv[1] = -m[1]  * m[10] * m[15] + 
              m[1]  * m[11] * m[14] + 
              m[9]  * m[2] * m[15] - 
              m[9]  * m[3] * m[14] - 
              m[13] * m[2] * m[11] + 
              m[13] * m[3] * m[10];

    inv[5] = m[0]  * m[10] * m[15] - 
             m[0]  * m[11] * m[14] - 
             m[8]  * m[2] * m[15] + 
             m[8]  * m[3] * m[14] + 
             m[12] * m[2] * m[11] - 
             m[12] * m[3] * m[10];

    inv[9] = -m[0]  * m[9] * m[15] + 
              m[0]  * m[11] * m[13] + 
              m[8]  * m[1] * m[15] - 
              m[8]  * m[3] * m[13] - 
              m[12] * m[1] * m[11] + 
              m[12] * m[3] * m[9];

    inv[13] = m[0]  * m[9] * m[14] - 
              m[0]  * m[10] * m[13] - 
              m[8]  * m[1] * m[14] + 
              m[8]  * m[2] * m[13] + 
              m[12] * m[1] * m[10] - 
              m[12] * m[2] * m[9];

    inv[2] = m[1]  * m[6] * m[15] - 
             m[1]  * m[7] * m[14] - 
             m[5]  * m[2] * m[15] + 
             m[5]  * m[3] * m[14] + 
             m[13] * m[2] * m[7] - 
             m[13] * m[3] * m[6];

    inv[6] = -m[0]  * m[6] * m[15] + 
              m[0]  * m[7] * m[14] + 
              m[4]  * m[2] * m[15] - 
              m[4]  * m[3] * m[14] - 
              m[12] * m[2] * m[7] + 
              m[12] * m[3] * m[6];

    inv[10] = m[0]  * m[5] * m[15] - 
              m[0]  * m[7] * m[13] - 
              m[4]  * m[1] * m[15] + 
              m[4]  * m[3] * m[13] + 
              m[12] * m[1] * m[7] - 
              m[12] * m[3] * m[5];

    inv[14] = -m[0]  * m[5] * m[14] + 
               m[0]  * m[6] * m[13] + 
               m[4]  * m[1] * m[14] - 
               m[4]  * m[2] * m[13] - 
               m[12] * m[1] * m[6] + 
               m[12] * m[2] * m[5];

    inv[3] = -m[1] * m[6] * m[11] + 
              m[1] * m[7] * m[10] + 
              m[5] * m[2] * m[11] - 
              m[5] * m[3] * m[10] - 
              m[9] * m[2] * m[7] + 
              m[9] * m[3] * m[6];

    inv[7] = m[0] * m[6] * m[11] - 
             m[0] * m[7] * m[10] - 
             m[4] * m[2] * m[11] + 
             m[4] * m[3] * m[10] + 
             m[8] * m[2] * m[7] - 
             m[8] * m[3] * m[6];
 
    inv[11] = -m[0] * m[5] * m[11] + 
               m[0] * m[7] * m[9] + 
               m[4] * m[1] * m[11] - 
               m[4] * m[3] * m[9] - 
               m[8] * m[1] * m[7] + 
               m[8] * m[3] * m[5];

    inv[15] = m[0] * m[5] * m[10] - 
              m[0] * m[6] * m[9] - 
              m[4] * m[1] * m[10] + 
              m[4] * m[2] * m[9] + 
              m[8] * m[1] * m[6] - 
              m[8] * m[2] * m[5];

    let det = 1.0 / (m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12]);

    for i in 0..16 {
        out[i] = inv[i] * det;
    }

    Matrix {
      values: out
    }
  }


  pub fn simple_ortho(width: f32, height: f32) -> Self {
    Matrix::ortho(0.0, width, 0.0, height, -1.0, 1.0)
  }

  pub fn ortho(left: f32, right: f32, bottom: f32, top: f32, near: f32, far: f32) -> Self {
    Matrix { values: [
      2.0 / (right - left), 0.0                 , 0.0                , -((right + left) / (right - left)),
      0.0                 , 2.0 / (top - bottom), 0.0                , -((top + bottom) / (top - bottom)),
      0.0                 , 0.0                 , -2.0 / (far - near), -((far + near) / (far - near)),
      0.0                 , 0.0                 ,  0.0               , 1.0
    ]}
  }
}

impl Vector2 {
  pub fn new(x: f32, y: f32) -> Self {
    Vector2 {x, y}
  }

  pub fn cross(&self, other: &Vector2) -> f32 {
    self.x * other.y - self.y * other.x
  }

  pub fn dot(&self, other: &Vector2) -> f32 {
    self.x * other.x + self.y + other.y
  }

  pub fn len(&self) -> f32 {
    (self.x*self.x + self.y*self.y).sqrt()
  }

  pub fn normalize(&mut self) {
    let len = self.len();
    self.x = self.x / len;
    self.y = self.y / len;
  }

  pub fn normalized(&self) -> Vector2 {
    let len = self.len();
    Vector2 {
      x: self.x / len,
      y: self.y / len,
    }
  }


}

pub fn vec2(x: f32, y: f32) -> Vector2 {
  Vector2::new(x, y)
}

impl_op_ex!(+ |a: &Vector2, b: &Vector2| -> Vector2 { Vector2 { x: a.x + b.x, y: a.y + b.y }});

impl PartialOrd for Vector2 {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    self.len().partial_cmp(&other.len())
  }
}


impl Sub for Vector2 {
  type Output = Vector2;

  fn sub(self, rhs: Vector2) -> Vector2 {
    Vector2 {
      x: self.x - rhs.x,
      y: self.y - rhs.y,
    }
  }
}

impl Mul<f32> for Vector2 {
  type Output = Vector2;

  fn mul(self, rhs: f32) -> Vector2 {
    Vector2 {
      x: self.x * rhs,
      y: self.y * rhs,
    }
  }
}

impl Vector {
  pub fn new(x: f32, y: f32, z: f32) -> Self {
    Vector {x, y, z}
  }

  pub fn dot(&self, v: &Vector) -> f32 {
    self.x * v.x + self.y * v.y + self.z * v.z
  }

  pub fn cross(&self, other: &Vector) -> Self {
    let v = self;
    let u = other;
    Vector {
      x: v.y * u.z - v.z * u.y,
      y: v.z * u.x - v.x * u.z,
      z: v.x * u.y - v.y * u.x
    }
  }

  pub fn len(&self) -> f32 {
    (self.x*self.x + self.y*self.y + self.z*self.z).sqrt()
  }
}

impl Add for Vector {
  type Output = Vector;

  fn add(self, rhs: Vector) -> Vector {
    Vector {
      x: self.x + rhs.x,
      y: self.y + rhs.y,
      z: self.z + rhs.z
    }
  }
}

impl Sub for Vector {
  type Output = Vector;

  fn sub(self, rhs: Vector) -> Vector {
    Vector {
      x: self.x - rhs.x,
      y: self.y - rhs.y,
      z: self.z - rhs.z
    }
  }
}

impl Mul<f32> for Vector {
  type Output = Vector;

  fn mul(self, rhs: f32) -> Vector {
    Vector {
      x: self.x * rhs,
      y: self.y * rhs,
      z: self.z * rhs
    }
  }
}

const IDENTITY_QUATERNION: Quaternion = Quaternion {
  x: 0.0,
  y: 0.0,
  z: 0.0,
  w: 1.0
};

impl Quaternion {
  pub fn identity() -> Self {
    IDENTITY_QUATERNION
  }

  pub fn from_axis_angle(axis: Vector, theta: f32) -> Self {
    let a = theta * 0.5;
    let s = a.sin();
    Quaternion {
      x: axis.x * s,
      y: axis.y * s,
      z: axis.z * s,
      w: a.cos()
    }
  }

  pub fn normalize(&mut self) {
    let len = self.x * self.x + self.y * self.y + self.z * self.z + self.w * self.w;
    let len = len.sqrt();
    let il = 1.0 / len;
    self.x += il;
    self.y += il;
    self.z += il;
    self.w += il;
  }

  pub fn normalized(&self) -> Self {
    let mut q2 = *self;
    q2.normalize();
    q2
  }

  pub fn to_matrix(&self) -> Matrix {
    let mut out = [0.0; 16];
    let q = self;
    let xx = q.x * q.x;
    let xy = q.x * q.y;
    let xz = q.x * q.z;
    let xw = q.x * q.w;

    let yy = q.y * q.y;
    let yz = q.y * q.z;
    let yw = q.y * q.w;

    let zz = q.z * q.z;
    let zw = q.z * q.w;

    out[0] = 1.0 - 2.0 * (yy + zz);
    out[1] = 2.0 * (xy - zw);
    out[2] = 2.0 * (xz + yw);
    out[3] = 0.0;

    out[4] = 2.0 * (xy + zw);
    out[5] = 1.0 - 2.0 * (xx + zz);
    out[6] = 2.0 * (yz - xw);
    out[7] = 0.0;

    out[8] = 2.0 * (xz - yw);
    out[9] = 2.0 * (yz + xw);
    out[10] = 1.0 - 2.0 * (xx + yy);
    out[11] = 0.0;

    out[12] = 0.0;
    out[13] = 0.0;
    out[14] = 0.0;
    out[15] = 1.0;

    Matrix {
      values: out
    }
  }

  pub fn rotate(&self, v: &Vector) -> Vector {
    let v3 = Vector::new(self.x, self.y, self.z);
    (v3 * v3.dot(v) * 2.0) + (*v * (self.w * self.w - v3.dot(&v3))) + (v3.cross(v) * self.w * 2.0)
  }
}

impl Mul for Quaternion {
  type Output = Quaternion;

  fn mul(self, rhs: Quaternion) -> Quaternion {
    let q0 = &self;
    let q1 = &rhs;
    Quaternion {
      x: q0.w * q1.x + q0.x * q1.w - q0.y * q1.z + q0.z * q1.y,
      y: q0.w * q1.y + q0.x * q1.z + q0.y * q1.w - q0.z * q1.x,
      z: q0.w * q1.z - q0.x * q1.y + q0.y * q1.x + q0.z * q1.w,
      w: q0.w * q1.w - q0.x * q1.x - q0.y * q1.y - q0.z * q1.z
    }
  }
}

impl Mul for Matrix {
  type Output = Matrix;

  fn mul(self, rhs: Matrix) -> Matrix {
    let m0 = &self.values;
    let m1 = &rhs.values;
    let mut out = [0.0; 16];

    out[0]	= m0[0] * m1[0]  + m0[4] * m1[1]  + m0[8]  * m1[2]  + m0[12] * m1[3];
    out[1]	= m0[1] * m1[0]  + m0[5] * m1[1]  + m0[9]  * m1[2]  + m0[13] * m1[3];
    out[2]	= m0[2] * m1[0]  + m0[6] * m1[1]  + m0[10] * m1[2]  + m0[14] * m1[3];
    out[3]	= m0[3] * m1[0]  + m0[7] * m1[1]  + m0[11] * m1[2]  + m0[15] * m1[3];

    out[4]	= m0[0] * m1[4]  + m0[4] * m1[5]  + m0[8]  * m1[6]  + m0[12] * m1[7];
    out[5]	= m0[1] * m1[4]  + m0[5] * m1[5]  + m0[9]  * m1[6]  + m0[13] * m1[7];
    out[6]	= m0[2] * m1[4]  + m0[6] * m1[5]  + m0[10] * m1[6]  + m0[14] * m1[7];
    out[7]	= m0[3] * m1[4]  + m0[7] * m1[5]  + m0[11] * m1[6]  + m0[15] * m1[7];

    out[8]	= m0[0] * m1[8]  + m0[4] * m1[9]  + m0[8]  * m1[10] + m0[12] * m1[11];
    out[9]	= m0[1] * m1[8]  + m0[5] * m1[9]  + m0[9]  * m1[10] + m0[13] * m1[11];
    out[10]	= m0[2] * m1[8]  + m0[6] * m1[9]  + m0[10] * m1[10] + m0[14] * m1[11];
    out[11]	= m0[3] * m1[8]  + m0[7] * m1[9]  + m0[11] * m1[10] + m0[15] * m1[11];

    out[12]	= m0[0] * m1[12] + m0[4] * m1[13] + m0[8]  * m1[14] + m0[12] * m1[15];
    out[13]	= m0[1] * m1[12] + m0[5] * m1[13] + m0[9]  * m1[14] + m0[13] * m1[15];
    out[14]	= m0[2] * m1[12] + m0[6] * m1[13] + m0[10] * m1[14] + m0[14] * m1[15];
    out[15]	= m0[3] * m1[12] + m0[7] * m1[13] + m0[11] * m1[14] + m0[15] * m1[15];

    Matrix {
      values: out
    }
  }
}

impl Mul<Quaternion> for Matrix {
  type Output = Matrix;

  fn mul(self, rhs: Quaternion) -> Matrix {
    let m2 = rhs.to_matrix();
    self * m2
  }
}

pub fn ortho(left: f32, right: f32, bottom: f32, top: f32, near: f32, far: f32) -> [f32; 16] {
  [
    2.0 / (right - left), 0.0                 , 0.0                , -((right + left) / (right - left)),
    0.0                 , 2.0 / (top - bottom), 0.0                , -((top + bottom) / (top - bottom)),
    0.0                 , 0.0                 , -2.0 / (far - near), -((far + near) / (far - near)),
    0.0                 , 0.0                 ,  0.0               , 1.0
  ]
}

