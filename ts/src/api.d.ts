
declare module "math" {
  export class Vector2F {
    constructor(x: number, y: number);
    x: number;
    y: number;

    add(vec: Vector2F): Vector2F;
    sub(vec: Vector2F): Vector2F;
  }

  export class Vector {
    constructor(x: number, y: number, z: number);
    x: number;
    y: number;
    z: number;

    add(vec: Vector): Vector;
    sub(vec: Vector): Vector;
  }

  export class Matrix {
    constructor();

    static translation(t: Vector): Matrix;
    static scale(t: Vector): Matrix;
    static simpleOrtho(width: number, height: number): Matrix;

    inverse(): Matrix;
    mul(mat: Matrix): Matrix;
  }

  export class RectF {
    constructor(origin: Vector2F, size: Vector2F);
  }
}

declare module "color" {
  export class Color {
    constructor();

    static fromHex(hex: string): Color;

    r: number;
    g: number;
    b: number;
    a: number;
  }
}

declare module "ui" {
  import type { Vector2F, RectF, Matrix } from "math";
  import type { Color } from "color";

  export class Canvas {
    constructor(size: Vector2F);
    color(color: Color): void;
    fillRect(rect: RectF): void;
    strokeRect(rect: RectF): void;
    resize(x: number, y: number): void
    text(t: string): void
    pushTranslation(translation: Vector2F): void;
    popTransform(): void;
  }

  export class UI {
    constructor(size: Vector2F);
    resize(x: number, y: number): void
    paint(c: Canvas): void
    render(m: Matrix): void
  }
}

declare module "" {
  global {
    enum MouseButton {
      Left = 0,
      Right = 1,
      Middle = 2,
      Other = 3
    }
    function log(...data: any[]): void;
  }
}
