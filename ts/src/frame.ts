import { Canvas } from "ui";
import { RectF, Vector2F } from "math";
import { Constraints, Region } from "./region";
import * as theme from "./theme";

export class Frame extends Region {
  rect: RectF;

  constructor(size: Vector2F, name?: string) {
    super(size, name);
    this.rect = new RectF(new Vector2F(0, 0), size);
  }

  titlebar(): RectF {
    return new RectF(new Vector2F(0.0, 0.0), new Vector2F(this.size.x, 24.0));
  }

  layout(c: Constraints): Vector2F {
    let size = c.constrain(this.size);
    this.rect = new RectF(new Vector2F(0, 0), size);
    return c.constrain(this.size);
  }

  paint(canvas: Canvas) {
    let titlebar = this.titlebar();
    canvas.color(theme.Default.button_bg);
    canvas.fillRect(this.rect);
    canvas.color(theme.Default.contrast_border);
    canvas.strokeRect(titlebar);
    canvas.strokeRect(this.rect);
    canvas.pushTranslation(new Vector2F(4.0, 16.0));
    canvas.text(this.name);
    canvas.popTransform();
  }
}
