
import { Matrix, Vector2F } from "math";
import { Canvas, UI } from "ui";
import { Constraints, Region } from "./region";
import { Frame } from "./frame";

let ORTHO = Matrix.simpleOrtho(500, 500);

let canvas = new Canvas(new Vector2F(500, 500));
let ui = new UI(new Vector2F(500, 500));

let root = new Region(new Vector2F(500, 500), "Root");
let frame = new Frame(new Vector2F(400, 400), "Frame 1");
root.add(frame);
frame.position = new Vector2F(100, 100);


function OnMouseDown(button: MouseButton) {
}

function OnMouseUp() {
}

function OnMouseMove(x: number, y: number, nx: number, ny: number) {
}

function Resize(x: number, y: number) {
  ui.resize(x, y);
  ORTHO = Matrix.simpleOrtho(x, y);
  canvas.resize(x, y);
  let size = new Vector2F(x, y);
  root.resize(size);
  let c = Constraints.of_max(size);
}

function OnPaint() {
  root.paint(canvas);
  ui.paint(canvas);
  ui.render(ORTHO);
}
 
globalThis.OnPaint = OnPaint;
globalThis.OnMouseDown = OnMouseDown;
globalThis.OnMouseUp = OnMouseUp;
globalThis.OnMouseMove = OnMouseMove;
globalThis.Resize = Resize;
