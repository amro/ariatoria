import { Canvas } from "ui";
import { Vector2F } from "math";

export class Constraints {
  min: Vector2F;
  max: Vector2F;

  constructor(min: Vector2F, max: Vector2F) {
    this.min = min;
    this.max = max;
  }

  static of_max(max: Vector2F): Constraints {
    return new Constraints(new Vector2F(0, 0), max);
  }

  fits(size: Vector2F): boolean {
    return (
      size.x <= this.max.x && size.x >= this.min.x &&
      size.y <= this.max.y && size.y >= this.min.y
    );
  }

  constrain(vec: Vector2F): Vector2F {
    let [x, y] = [vec.x, vec.y];
    let [min_x, min_y] = [this.min.x, this.min.y];
    let [max_x, max_y] = [this.max.x, this.max.y];

    if (x > max_x) {
      x = max_x;
    }
    
    if (x < min_x) {
      x = min_x;
    }

    if (y > max_y) {
      y = max_y;
    }
    
    if (y < min_y) {
      y = min_y;
    }

    return new Vector2F(x, y);
  }

  toString(): string {
    return this.min.toString() + " - " + this.max.toString();
  }
}

export class Region {
  children: Array<Region>;
  size: Vector2F;
  position: Vector2F;
  name: string;

  constructor(size: Vector2F, name?: string) {
    this.size = size;
    this.position = new Vector2F(0, 0);
    this.children = new Array<Region>();
    this.name = name || "unnamed";
  }

  resize(size: Vector2F) {
    this.size = size;
  }

  add(child: Region) {
    this.children.push(child);
  }

  layout(c: Constraints): Vector2F {
    log("sizzle ", this.size);
    let size = c.constrain(this.size);
    for (let child of this.children) {
      // child.layout(Constraints.of_max(size.sub(child.position)));
      child.layout(c);
    }
    return new Vector2F(10, 10);
  }

  paint(canvas: Canvas) {
    for (let child of this.children) {
      canvas.pushTranslation(child.position);
      child.paint(canvas);
      canvas.popTransform();
    }
  }
}