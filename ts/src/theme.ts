
import { Color } from 'color'

export type Theme = {
  bg: Color,
  deep_bg: Color,
  button_bg: Color,
  button_bg_disabled: Color,
  button_border: Color,
  tab_border: Color,
  contrast_border: Color,
}

export const Default: Theme = {
  bg: Color.fromHex("#333B4F"),
  deep_bg: Color.fromHex("#202531"),
  button_bg: Color.fromHex("#262C3B"),
  button_bg_disabled: Color.fromHex("#2E3547"),
  button_border: Color.fromHex("#202531"),
  tab_border: Color.fromHex("#191D28"),
  contrast_border: Color.fromHex("#FFFFFF"),
}
